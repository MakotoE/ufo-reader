FROM golang:1.11.4-alpine3.8
ARG VERSION
COPY . /go/src/gitlab.com/MakotoE/ufo-reader
RUN ["sh", "-c", "go build -o /ufoweb -ldflags \"-X gitlab.com/MakotoE/ufo-reader/pkg/version.Version=$VERSION\" /go/src/gitlab.com/MakotoE/ufo-reader/cmd/ufoweb/ufoweb.go"]

FROM alpine:3.8
COPY --from=0 /ufoweb /
EXPOSE 8000
ENTRYPOINT ["/ufoweb"]