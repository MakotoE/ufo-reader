package mysql

import (
	"database/sql"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/MakotoE/ufo-reader/pkg/mysql"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

type MySQLTest struct {
	suite.Suite
	database *Database
}

func TestMySQL(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	suite.Run(t, &MySQLTest{})
}

func (this *MySQLTest) SetupSuite() {
	var err error
	if this.database, err = NewMySQLDatabase(); err != nil {
		panic(err)
	}
	if err = PrepareTables(); err != nil {
		this.database.Close()
		panic(err)
	}
}

func (this *MySQLTest) TearDownSuite() {
	this.database.Close()
}

var options = map[string]interface{}{
	"User":   "root",
	"Passwd": "",
	"Addr":   "localhost:3306",
	"DBName": "testDatabase",
	"Table":  "testTable",
}

func (this *MySQLTest) TestRead() {
	rawBytes := func(s string) *sql.RawBytes {
		b := sql.RawBytes(s)
		return &b
	}

	tests := []struct {
		range1D     reader.Range1D
		expected    readresult.Result1D
		expectError bool
	}{
		{
			reader.Range1D{0, 0},
			readresult.Object1D(nil),
			false,
		},
		{
			reader.Range1D{0, 2},
			readresult.Object1D{
				{"ID": &sql.NullInt64{0, true}, "name": rawBytes("name0")},
				{"ID": &sql.NullInt64{1, true}, "name": rawBytes("name1")},
			},
			false,
		},
		{
			reader.Range1D{0, 3},
			readresult.Object1D{
				{"ID": &sql.NullInt64{0, true}, "name": rawBytes("name0")},
				{"ID": &sql.NullInt64{1, true}, "name": rawBytes("name1")},
			},
			false,
		},
	}

	m, err := (&mysql.MySQL{}).Init(options)
	require.Nil(this.T(), err)

	for _, test := range tests {
		result, err := m.(*mysql.MySQL).Read(test.range1D)
		this.Equal(test.expected, result)
		testutils.CheckError(this.T(), test.expectError, err)
	}
}

func (this *MySQLTest) TestShape() {
	m, err := (&mysql.MySQL{}).Init(options)
	require.Nil(this.T(), err)

	result, err := m.(*mysql.MySQL).Shape()
	this.Equal([1]int{2}, result)
	this.Nil(err)
}
