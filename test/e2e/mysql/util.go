package mysql

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
	"os"
	"time"
)

type Database struct {
	Context context.Context
	Client  *client.Client
	ID      string
}

func NewMySQLDatabase() (*Database, error) {
	client, err := client.NewEnvClient()
	if err != nil {
		return nil, errors.Wrap(err, "NewEnvClient")
	}

	imageName := "docker.io/library/mysql:8.0"
	context := context.Background()
	_, err = client.ImagePull(context, imageName, types.ImagePullOptions{})
	if err != nil {
		return nil, errors.Wrap(err, "ImagePull")
	}

	containerConfig := &container.Config{
		Image: imageName,
		Env:   []string{"MYSQL_ALLOW_EMPTY_PASSWORD=yes"},
	}
	hostConfig := &container.HostConfig{
		PortBindings: nat.PortMap{
			"3306/tcp": []nat.PortBinding{{HostIP: "localhost", HostPort: "3306"}},
		},
	}
	resp, err := client.ContainerCreate(context, containerConfig, hostConfig, nil, "")
	if err != nil {
		return nil, errors.Wrap(err, "ContainerCreate")
	}

	if err := client.ContainerStart(context, resp.ID, types.ContainerStartOptions{}); err != nil {
		return nil, errors.Wrap(err, "ContainerStart")
	}

	time.Sleep(time.Second * 20) // Wait for MySQL to finish initialization
	return &Database{context, client, resp.ID}, nil
}

func (this *Database) Close() {
	err := this.Client.ContainerStop(this.Context, this.ID, nil)
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "ContainerStop:", err.Error())
	}
	_ = this.Client.Close()
}

func PrepareTables() error {
	config := &mysql.Config{User: "root"}
	db, err := sql.Open("mysql", config.FormatDSN())
	if err != nil {
		return errors.Wrap(err, "sql.Open")
	}

	queries := []string{
		"CREATE DATABASE testDatabase",
		"USE testDatabase",
		"CREATE TABLE testTable(ID INTEGER, name VARCHAR(100))",
		"INSERT INTO testTable VALUES(0, 'name0')",
		"INSERT INTO testTable VALUES(1, 'name1')",
	}

	for _, query := range queries {
		if _, err := db.Exec(query); err != nil {
			return errors.Wrap(err, "Query")
		}
	}
	return nil
}
