package desktop

func Fuzz(data []byte) int {
	backend, err := NewBackend()
	if err != nil {
		panic(err)
	}
	defer backend.Close()

	_, _ = backend.Stdin.Write(data)
	_, _ = backend.Stdin.Write(data)
	if backend.Cmd.Process == nil {
		panic("ufodesktop is not running")
	}
	return 0
}
