#!/usr/bin/env bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

rmdir -rf ${parent_path}/corpus
rmdir -rf ${parent_path}/crashers
rmdir -rf ${parent_path}/suppressions
go-fuzz-build -o ${parent_path}/desktop-fuzz.zip gitlab.com/MakotoE/ufo-reader/test/e2e/desktop
go-fuzz -bin=${parent_path}/desktop-fuzz.zip -workdir=${parent_path} -procs=1
