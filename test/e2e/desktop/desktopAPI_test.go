package desktop

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"os/exec"
	"path/filepath"
	"runtime"
	"testing"
)

func TestVersionFlag(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	command := "go run " +
		"-ldflags '-X gitlab.com/MakotoE/ufo-reader/pkg/version.Version=versionNumber' " +
		"../../../cmd/ufodesktop/ufodesktop.go --version"
	out, err := exec.Command("sh", "-c", command).Output()
	assert.Equal(t, []byte("versionNumber\n"), out)
	assert.Nil(t, err)
}

type DesktopAPITest struct {
	suite.Suite
	Backend *Backend
}

func TestDesktopAPI(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	suite.Run(t, &DesktopAPITest{})
}

func (this *DesktopAPITest) SetupSuite() {
	if err := Build(); err != nil {
		panic(err)
	}

	backend, err := NewBackend()
	if err != nil {
		panic(err)
	}

	this.Backend = backend
}

func (this *DesktopAPITest) TearDownSuite() {
	this.Backend.Close()
}

func (this *DesktopAPITest) TestVersion() {
	require.Nil(this.T(), this.Backend.SendMessage(&message.Version{message.Message{"Version"}}))
	output, err := this.Backend.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(message.NewOutput(-1, map[string]interface{}{"Version": "versionNumber"}), output)
}

func (this *DesktopAPITest) TestFileChainRead() {
	_, currPath, _, _ := runtime.Caller(0)
	path := filepath.Dir(currPath) + "/../../../test/testFiles/1byte"

	// Send chain
	message0 := &message.Chain{
		message.Message{"Chain"},
		0,
		[]message.ReaderListItem{{"File", map[string]interface{}{"Path": path}}},
	}
	require.Nil(this.T(), this.Backend.SendMessage(message0))

	// Read
	r := map[string]interface{}{"From": 0, "Size": 1}
	message1 := &message.Read{message.Message{"Read"}, 0, 0, r}
	require.Nil(this.T(), this.Backend.SendMessage(message1))

	// Get output
	output, err := this.Backend.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(message.NewOutput(0, "YQ=="), output)
}

func (this *DesktopAPITest) TestShape() {
	message0 := &message.Chain{
		message.Message{"Chain"},
		0,
		[]message.ReaderListItem{
			{Name: "Text", Options: map[string]interface{}{"Str": "a"}},
		},
	}
	require.Nil(this.T(), this.Backend.SendMessage(message0))

	message1 := &message.Shape{message.Message{"Shape"}, 0, 0}
	require.Nil(this.T(), this.Backend.SendMessage(message1))

	output, err := this.Backend.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(message.NewOutput(0, []interface{}{1.0}), output)
}

func (this *DesktopAPITest) TestSubscribe() {
	message0 := &message.Chain{
		message.Message{"Chain"},
		0,
		[]message.ReaderListItem{{"CurrentDate", nil}},
	}
	require.Nil(this.T(), this.Backend.SendMessage(message0))

	message1 := &message.Subscribe{message.Message{"Subscribe"}, 0, 0, nil}
	require.Nil(this.T(), this.Backend.SendMessage(message1))

	output, err := this.Backend.ReadMessage()
	require.Nil(this.T(), err)
	_, ok := output.Output.(map[string]interface{})["Year"]
	this.True(ok)
}

func (this *DesktopAPITest) TestErrors() {
	_, _ = this.Backend.Stdin.Write([]byte("a"))
	output, err := this.Backend.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(-1, output.ChainID)
	this.Contains(output.Output.(map[string]interface{})["Error"], "prefix")

	message := &message.File{message.Message{"File"}, 0, "file0"}
	require.Nil(this.T(), this.Backend.SendMessage(message))

	output, err = this.Backend.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(-1, output.ChainID)
	this.Contains(output.Output.(map[string]interface{})["Error"], "does not exist")
}
