package desktop

import (
	"encoding/binary"
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/ufodesktop"
	"io"
	"os"
	"os/exec"
)

type Backend struct {
	Cmd    *exec.Cmd
	Stdin  io.WriteCloser
	Stdout io.ReadCloser
}

func Build() error {
	command := "go build " +
		"-ldflags '-X gitlab.com/MakotoE/ufo-reader/pkg/version.Version=versionNumber' " +
		"../../../cmd/ufodesktop/ufodesktop.go"
	err := exec.Command("sh", "-c", command).Run()
	if err != nil {
		return errors.Wrap(err, "Run")
	}
	return nil
}

func NewBackend() (*Backend, error) {
	cmd := exec.Command("./ufodesktop")

	stdin, err := cmd.StdinPipe()
	if err != nil {
		return nil, errors.Wrap(err, "StdinPipe")
	}

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, errors.Wrap(err, "StdoutPipe")
	}

	cmd.Stderr = os.Stderr
	err = cmd.Start()
	if err != nil {
		return nil, errors.Wrap(err, "cmd.Start")
	}

	return &Backend{cmd, stdin, stdout}, nil
}

func (this *Backend) Close() {
	if this.Cmd.Process != nil {
		_ = this.Cmd.Process.Signal(os.Interrupt)
		_ = this.Cmd.Wait()
	}
}

func (this *Backend) SendMessage(msg message.IMessage) error {
	b, err := json.Marshal(msg)
	if err != nil {
		return errors.Wrap(err, "json.Marshal")
	}

	_, err = this.Stdin.Write(ufodesktop.PrefixLength(b))
	return errors.Wrap(err, "Write")
}

func (this *Backend) ReadMessage() (*message.Output, error) {
	n, err := ReadDataLength(this.Stdout)
	if err != nil {
		return nil, errors.Wrap(err, "ReadDataLength")
	}

	data := make([]byte, n)
	if _, err := this.Stdout.Read(data); err != nil {
		return nil, errors.Wrap(err, "os.Stdin.Read")
	}

	msg, err := message.Deserialize(data)
	if err != nil {
		return nil, errors.Wrap(err, "Deserialize")
	}
	return msg.(*message.Output), nil
}

func ReadDataLength(stdout io.ReadCloser) (int, error) {
	prefix := make([]byte, 8)
	n, err := stdout.Read(prefix)
	if err != nil {
		return 0, errors.Wrap(err, "Read")
	}
	if n != 8 {
		return 0, errors.New("prefix has unexpected number of bytes")
	}

	return int(binary.LittleEndian.Uint64(prefix)), nil
}
