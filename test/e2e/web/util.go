package web

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"os"
	"os/exec"
	"time"
)

type Server struct {
	Process *os.Process
}

func NewServer() (*Server, error) {
	command := "go build " +
		"-ldflags '-X gitlab.com/MakotoE/ufo-reader/pkg/version.Version=versionNumber' " +
		"../../../cmd/ufoweb/ufoweb.go"
	err := exec.Command("sh", "-c", command).Run()
	if err != nil {
		return nil, errors.Wrap(err, "Run")
	}
	cmd := exec.Command("./ufoweb")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Start()
	if err != nil {
		return nil, errors.Wrap(err, "cmd.Start")
	}

	time.Sleep(time.Millisecond * 50)
	return &Server{cmd.Process}, nil
}

func (this *Server) Close() {
	if this.Process != nil {
		_ = this.Process.Signal(os.Interrupt)
		_, _ = this.Process.Wait()
	}
}

type Connection struct {
	Connection *websocket.Conn
}

func NewConnection() (*Connection, error) {
	connection, _, err := websocket.DefaultDialer.Dial("ws://localhost:8000", nil)
	if err != nil {
		return nil, errors.Wrap(err, "Dial")
	}
	return &Connection{connection}, nil
}

func (this *Connection) SendMessage(msg message.IMessage) error {
	b, err := json.Marshal(msg)
	if err != nil {
		return errors.Wrap(err, "json.Marshal")
	}

	err = this.Connection.WriteMessage(websocket.TextMessage, b)
	return errors.Wrap(err, "WriteMessage")
}

func (this *Connection) SendBinary(b []byte) error {
	err := this.Connection.WriteMessage(websocket.BinaryMessage, b)
	return errors.Wrap(err, "WriteMessage")
}

func (this *Connection) ReadMessage() (*message.Output, error) {
	_, b, err := this.Connection.ReadMessage()
	if err != nil {
		return nil, errors.Wrap(err, "ReadMessage")
	}

	msg, err := message.Deserialize(b)
	if err != nil {
		return nil, errors.Wrap(err, "Deserialize")
	}

	output, ok := msg.(*message.Output)
	if !ok {
		return nil, errors.New("output is not *message.Output: " + fmt.Sprintf("%+v", msg))
	}

	return output, nil
}
