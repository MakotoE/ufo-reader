package web

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"io/ioutil"
	"os/exec"
	"testing"
	"time"
)

func TestVersionFlag(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	command := "go run " +
		"-ldflags '-X gitlab.com/MakotoE/ufo-reader/pkg/version.Version=versionNumber' " +
		"../../../cmd/ufoweb/ufoweb.go --version"
	out, err := exec.Command("sh", "-c", command).Output()
	assert.Equal(t, []byte("versionNumber\n"), out)
	assert.Nil(t, err)
}

type WebAPITest struct {
	suite.Suite
	Server *Server
}

func TestAPI(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	suite.Run(t, &WebAPITest{})
}

func (this *WebAPITest) SetupSuite() {
	var err error
	if this.Server, err = NewServer(); err != nil {
		panic(err)
	}
}

func (this *WebAPITest) TearDownSuite() {
	this.Server.Close()
}

func (this *WebAPITest) TestVersion() {
	connection, err := NewConnection()
	require.Nil(this.T(), err)

	require.Nil(this.T(), connection.SendMessage(&message.Version{message.Message{"Version"}}))
	output, err := connection.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(message.NewOutput(-1, map[string]interface{}{"Version": "versionNumber"}), output)
}

func (this *WebAPITest) TestFileChainRead() {
	connection, err := NewConnection()
	require.Nil(this.T(), err)

	// Send file info
	message0 := &message.File{message.Message{"File"}, 0, "file0"}
	require.Nil(this.T(), connection.SendMessage(message0))

	// Send file data
	require.Nil(this.T(), connection.SendBinary([]byte("a,b")))

	// Send chain
	message1 := &message.Chain{
		message.Message{"Chain"},
		0,
		[]message.ReaderListItem{
			{Name: "File", Options: map[string]interface{}{"Path": "file0"}},
		},
	}
	require.Nil(this.T(), connection.SendMessage(message1))

	// Read
	message2 := &message.Read{
		message.Message{"Read"},
		0,
		0,
		map[string]interface{}{"From": 0, "Size": 3},
	}
	require.Nil(this.T(), connection.SendMessage(message2))

	// Get output
	output, err := connection.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(message.NewOutput(0, "YSxi"), output)
}

func (this *WebAPITest) TestRepeatedFile() {
	connection, err := NewConnection()
	require.Nil(this.T(), err)

	require.Nil(this.T(), connection.SendBinary(nil))
	require.Nil(this.T(), connection.SendBinary(nil))

	output, err := connection.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(-1, output.ChainID)
	this.Contains(output.Output.(map[string]interface{})["Error"], "file exists")
}

func (this *WebAPITest) TestShape() {
	connection, err := NewConnection()
	require.Nil(this.T(), err)

	message0 := &message.Chain{
		message.Message{"Chain"},
		0,
		[]message.ReaderListItem{
			{Name: "Text", Options: map[string]interface{}{"Str": "a"}},
		},
	}
	require.Nil(this.T(), connection.SendMessage(message0))

	message1 := &message.Shape{
		message.Message{"Shape"},
		0,
		0,
	}
	require.Nil(this.T(), connection.SendMessage(message1))

	output, err := connection.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(message.NewOutput(0, []interface{}{1.0}), output)
}

func (this *WebAPITest) TestClose() {
	connection, err := NewConnection()
	require.Nil(this.T(), err)

	require.Nil(this.T(), connection.SendBinary(nil))
	connection.Connection.Close()
	time.Sleep(time.Millisecond * 100)

	files, err := ioutil.ReadDir("files")
	assert.Equal(this.T(), 0, len(files))
}

func (this *WebAPITest) TestSubscribe() {
	connection, err := NewConnection()
	require.Nil(this.T(), err)

	message0 := &message.Chain{
		message.Message{"Chain"},
		0,
		[]message.ReaderListItem{{"CurrentDate", nil}},
	}
	require.Nil(this.T(), connection.SendMessage(message0))

	message1 := &message.Subscribe{message.Message{"Subscribe"}, 0, 0, nil}
	require.Nil(this.T(), connection.SendMessage(message1))

	output, err := connection.ReadMessage()
	require.Nil(this.T(), err)
	_, ok := output.Output.(map[string]interface{})["Year"]
	this.True(ok)
}

func (this *WebAPITest) TestErrors() {
	connection, err := NewConnection()
	require.Nil(this.T(), err)

	require.Nil(this.T(), connection.SendMessage(&message.Output{Message: message.Message{"Output"}}))
	output, err := connection.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(-1, output.ChainID)
	this.Contains(output.Output.(map[string]interface{})["Error"], "message type does not exist")

	message0 := &message.Read{
		message.Message{"Read"},
		0,
		0,
		map[string]interface{}{"Low": 0, "High": 0},
	}
	err = connection.SendMessage(message0)
	require.Nil(this.T(), err)
	output, err = connection.ReadMessage()
	require.Nil(this.T(), err)
	this.Equal(-1, output.ChainID)
	this.Contains(output.Output.(map[string]interface{})["Error"], "index does not exist: 0")
}
