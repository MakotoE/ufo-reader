package integration

import (
	"encoding/csv"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/chain"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/pkg/sessiondesktop"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"strings"
	"testing"
	"time"
)

func TestTextCSVSelectorCount(t *testing.T) {
	builder := &strings.Builder{}
	csvWriter := csv.NewWriter(builder)
	records := [][]string{{"a", "b"}, {"0", "1"}, {"0", "1"}, {"2", "3"}}
	err := csvWriter.WriteAll(records)
	require.Nil(t, err)
	csvWriter.Flush()

	readerList := []message.ReaderListItem{
		{"Text", map[string]interface{}{"Str": builder.String()}},
		{"CSV", map[string]interface{}{"ContainsHeader": true}},
		{"Selector", map[string]interface{}{"Keys": []string{"a"}}},
		{"Count", nil},
	}

	output := make(chan *message.Output, 1)
	c := chain.New(0, output)
	c.Update(readerList)

	c.Read(3, map[string]interface{}{"From": 0, "Size": 2})

	select {
	case result := <-output:
		expected := message.NewOutput(0, readresult.Object1D{
			map[string]interface{}{"Value": map[string]interface{}{"a": "2"}, "Count": 1},
			map[string]interface{}{"Value": map[string]interface{}{"a": "0"}, "Count": 2},
		})
		assert.Equal(t, expected, result)
	default:
		assert.Fail(t, "did not receive output")
	}
}

func TestSubscribeCurrentDate(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	output := make(chan *message.Output, 1)
	session := sessiondesktop.New(output)
	message0 := &message.Chain{message.Message{"Chain"}, 0, []message.ReaderListItem{
		{"CurrentDate", nil},
	}}
	session.Input(message0)

	message1 := &message.Subscribe{message.Message{"Subscribe"}, 0, 0, nil}
	go session.Input(message1)

	ticks := 0
	for {
		select {
		case msg := <-output:
			_, ok := msg.Output.(readresult.Object0D)["Year"]
			assert.True(t, ok)
			ticks++
			if ticks == 2 {
				session.Close()
				return
			}
		case <-time.After(time.Second * 2):
			assert.Fail(t, "did not receive output")
		}
	}
}

func TestTextThenCurrentDate(t *testing.T) {
	output := make(chan *message.Output, 1)
	session := sessiondesktop.New(output)
	message0 := &message.Chain{message.Message{"Chain"}, 0, []message.ReaderListItem{
		{"Text", map[string]interface{}{"Str": ""}},
	}}
	session.Input(message0)

	message1 := &message.Chain{message.Message{"Chain"}, 0, []message.ReaderListItem{
		{"CurrentDate", nil},
	}}
	session.Input(message1)

	message2 := &message.Read{message.Message{"Read"}, 0, 0, nil}
	session.Input(message2)

	select {
	case msg := <-output:
		_, ok := msg.Output.(readresult.Object0D)["Year"]
		assert.True(t, ok)
	default:
		assert.Fail(t, "did not receive output")
	}
}

func TestFilePNGReshape2DIndex2DAverageObject(t *testing.T) {
	readerList := []message.ReaderListItem{
		{"File", map[string]interface{}{"Path": testutils.Dir() + "/2x2.png"}},
		{"PNG", nil},
		{"Reshape2D", map[string]interface{}{"Axis": 0, "AxisLength": 1}},
		{"Index2D", map[string]interface{}{"Row": 0}},
		{"AverageObject", nil},
	}

	output := make(chan *message.Output, 1)
	c := chain.New(0, output)
	c.Update(readerList)

	c.Read(4, map[string]interface{}{"Size": 4})

	select {
	case result := <-output:
		expected := message.NewOutput(0, readresult.Object0D{
			"R": 16383.75,
			"G": 0.0,
			"B": 16383.75,
			"A": 32767.5,
		})
		assert.Equal(t, expected, result)
	default:
		assert.Fail(t, "did not receive output")
	}
}
