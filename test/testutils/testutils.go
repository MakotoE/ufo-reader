package testutils

import (
	"github.com/stretchr/testify/assert"
	"path/filepath"
	"runtime"
	"testing"
)

func Dir() string {
	_, file, _, _ := runtime.Caller(0)
	return filepath.Clean(filepath.Dir(file) + "../../../test/testFiles")
}

func CheckError(t *testing.T, expectError bool, err error) {
	if expectError {
		assert.NotNil(t, err)
	} else {
		assert.Nil(t, err)
	}
}
