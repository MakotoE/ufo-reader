package sessionweb_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/chain"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/session"
	"gitlab.com/MakotoE/ufo-reader/pkg/sessionweb"
	"gitlab.com/MakotoE/ufo-reader/pkg/text"
	"testing"
)

func TestDirName(t *testing.T) {
	result := sessionweb.DirName("a", 0)
	assert.Equal(t, "ThGV3wIN5Z4NZaM6QnnxGD565OXZgOMJ-LVa3_LmHD4=", result)
}

func TestSession_HandleChain(t *testing.T) {
	tests := []struct {
		initialChains  session.ChainArray
		msg            *message.Chain
		expectedChains session.ChainArray
	}{
		{
			session.ChainArray{},
			&message.Chain{ChainID: 0},
			session.ChainArray{0: {ChainID: 0}},
		},
		{
			session.ChainArray{},
			&message.Chain{ChainID: 0, ReaderList: []message.ReaderListItem{}},
			session.ChainArray{0: {ChainID: 0, ReaderList: []message.ReaderListItem{}}},
		},
		{
			session.ChainArray{},
			&message.Chain{ChainID: 1, ReaderList: []message.ReaderListItem{}},
			session.ChainArray{1: {ChainID: 1, ReaderList: []message.ReaderListItem{}}},
		},
		{
			session.ChainArray{},
			&message.Chain{
				ChainID: 0,
				ReaderList: []message.ReaderListItem{
					{Name: "Text", Options: map[string]interface{}{"Str": "Str"}},
				},
			},
			session.ChainArray{
				0: {
					ChainID: 0,
					Readers: chain.ReaderArray{&text.Text{"Str"}},
					ReaderList: []message.ReaderListItem{
						{Name: "Text", Options: map[string]interface{}{"Str": "Str"}},
					},
				},
			},
		},
	}

	for _, test := range tests {
		session := &sessionweb.SessionWeb{Session: &session.Session{Chains: test.initialChains}}
		session.Chain(test.msg)
		for _, c := range session.Chains {
			c.UnsubscribeChan = nil
		}
		assert.Equal(t, test.expectedChains, session.Chains)
	}
}

func TestEditFilepath(t *testing.T) {
	tests := []struct {
		readerList         []message.ReaderListItem
		expectedReaderList []message.ReaderListItem
	}{
		{
			[]message.ReaderListItem{{Name: "File", Options: nil}},
			[]message.ReaderListItem{{Name: "File", Options: nil}},
		},
		{
			[]message.ReaderListItem{{Name: "File", Options: map[string]interface{}{"Path": "path"}}},
			[]message.ReaderListItem{
				{
					Name: "File",
					Options: map[string]interface{}{
						"Path": "files/X-zrZv_IbzjZUnhsbWlsecLbwjndTpG0ZynXOif7V-k=/path",
					},
				},
			},
		},
	}

	for _, test := range tests {
		sessionweb.EditFilepath("", 0, test.readerList)
		assert.Equal(t, test.expectedReaderList, test.readerList)
	}
}
