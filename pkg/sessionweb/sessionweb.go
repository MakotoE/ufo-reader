package sessionweb

import (
	"crypto/sha256"
	"encoding/base64"
	"github.com/matoous/go-nanoid"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/logger"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/session"
	"io"
	"os"
	"strconv"
)

const FilesDir = "files"

type SessionWeb struct {
	*session.Session
	SessionID      string
	UploadedChains []int
	NextFileInfo   *message.File
}

func New(output chan<- *message.Output) (*SessionWeb, error) {
	sessionID, err := gonanoid.Nanoid()
	if err != nil {
		return nil, err
	}

	newSession := &SessionWeb{
		&session.Session{session.ChainArray{}, output, &logger.Logger{}},
		sessionID,
		nil,
		&message.File{ChainID: 0, Name: "unnamed"},
	}
	return newSession, nil
}

func (this *SessionWeb) Close() {
	this.Session.Close()
	for _, chainID := range this.UploadedChains {
		_ = os.RemoveAll(FilesDir + "/" + DirName(this.SessionID, chainID))
	}
}

func (this *SessionWeb) Input(msg message.IMessage) {
	this.Logger.Push(msg)
	if err := this.RunFunction(msg); err != nil {
		this.OutputChan <- message.NewError(errors.Wrap(err, "RunFunction"))
	}
}

func (this *SessionWeb) RunFunction(msg message.IMessage) error {
	switch msg.GetID() {
	case "File":
		this.File(msg.(*message.File))
		return nil
	case "Chain":
		this.Chain(msg.(*message.Chain))
		return nil
	default:
		return this.RunBaseFunction(msg)
	}
}

func (this *SessionWeb) AddFile(data io.Reader) {
	this.UploadedChains = append(this.UploadedChains, this.NextFileInfo.ChainID)

	dirName := DirName(this.SessionID, this.NextFileInfo.ChainID)
	if err := os.Mkdir(FilesDir+"/"+dirName, 0700); err != nil && !os.IsExist(err) {
		this.OutputChan <- message.NewError(errors.Wrap(err, "Mkdir"))
	}

	filepath := FilesDir + "/" + dirName + "/" + this.NextFileInfo.Name
	file, err := os.OpenFile(filepath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		this.OutputChan <- message.NewError(errors.Wrap(err, "OpenFile"))
	}
	defer file.Close()

	_, err = io.Copy(file, data)
	if err != nil {
		this.OutputChan <- message.NewError(errors.Wrap(err, "io.Copy"))
	}
}

func (this *SessionWeb) File(msg *message.File) {
	this.NextFileInfo = msg
}

func DirName(sessionID string, chainID int) string {
	hash := sha256.Sum256([]byte(sessionID + strconv.Itoa(chainID)))
	hashStr := make([]byte, 44)
	base64.URLEncoding.Encode(hashStr, hash[:])
	return string(hashStr)
}

func (this *SessionWeb) Chain(msg *message.Chain) {
	EditFilepath(this.SessionID, msg.ChainID, msg.ReaderList)
	this.Chains.GetOrAdd(msg.ChainID, this.OutputChan).Update(msg.ReaderList)
}

func EditFilepath(sessionID string, chainID int, readerList []message.ReaderListItem) {
	for i := range readerList {
		if path, ok := readerList[i].Options["Path"]; ok {
			if pathStr, ok := path.(string); ok {
				newPath := FilesDir + "/" + DirName(sessionID, chainID) + "/" + pathStr
				readerList[i].Options["Path"] = newPath
			}
		}
	}
}
