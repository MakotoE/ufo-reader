package ufoweb

//go:generate mockgen -source=ufoweb.go -destination=../../test/mocks/mock_ufoweb/mock_ufoweb.go

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/sessionweb"
	"gitlab.com/MakotoE/ufo-reader/pkg/version"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
)

func RootHandler(w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.Upgrader{
		ReadBufferSize:  4096,
		WriteBufferSize: 4096,
		CheckOrigin:     CheckOrigin,
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		if !strings.Contains(err.Error(), "client is not using the websocket protocol") {
			_, _ = fmt.Fprintln(os.Stderr, errors.Wrap(err, "Upgrade").Error())
		}
		return
	}

	output := make(chan *message.Output, 5)
	exit := make(chan bool)
	go OutputLoop(conn, output, exit)

	session, err := sessionweb.New(output)
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, errors.Wrap(err, "NewSession").Error())
		return
	}
	defer func() { exit <- true }()
	defer session.Close()

	for {
		messageType, r, err := conn.NextReader()
		if err != nil {
			break
		}

		if err := ProcessMessage(session, messageType, r); err != nil {
			output <- message.NewError(errors.Wrap(err, "ProcessMessage"))
		}
	}
}

func CheckOrigin(r *http.Request) bool {
	return true
}

type Conn interface {
	WriteMessage(messageType int, data []byte) error
}

func OutputLoop(conn Conn, output <-chan *message.Output, exit <-chan bool) {
	for {
		select {
		case msg := <-output:
			WriteMessage(conn, msg)
		case <-exit:
			return
		}
	}
}

func WriteMessage(conn Conn, msg *message.Output) {
	b, err := json.Marshal(msg)
	if err != nil {
		b, err = json.Marshal(message.NewError(errors.Wrap(err, "json.Marshal")))
		if err != nil {
			panic(err)
		}
	}

	err = conn.WriteMessage(websocket.TextMessage, b)
	if err != nil && !strings.Contains(err.Error(), "broken pipe") {
		_, _ = fmt.Fprintln(os.Stderr, errors.Wrap(err, "WriteMessage").Error())
		return
	}
}

func ProcessMessage(session *sessionweb.SessionWeb, messageType int, r io.Reader) error {
	if messageType == websocket.TextMessage {
		data, err := ioutil.ReadAll(r)
		if err != nil {
			return errors.Wrap(err, "ReadAll")
		}

		msg, err := message.Deserialize(data)
		if err != nil {
			return errors.Wrap(err, "Deserialize")
		}

		session.Input(msg)
		return nil
	} else if messageType == websocket.BinaryMessage {
		session.AddFile(r)
		return nil
	}

	return errors.New("unsupported message type: " + strconv.Itoa(messageType))
}

func RunServer() {
	_ = os.Mkdir(sessionweb.FilesDir, 0700)

	// Files are deleted on connection close, but on SIGINT, sessions exit without cleaning up.
	defer os.RemoveAll(sessionweb.FilesDir)

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	server := http.Server{Addr: ":8000"}
	defer server.Close()

	http.HandleFunc("/", RootHandler)
	go func() {
		_, _ = os.Stdout.Write([]byte("listening\n"))
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			_, _ = fmt.Fprintf(os.Stderr, errors.Wrap(err, "ListenAndServe").Error())
		}
		stop <- os.Interrupt
	}()

	<-stop
}

func Main() {
	printVersion := flag.Bool("version", false, "prints version")
	flag.Parse()
	if *printVersion {
		_, _ = fmt.Fprintln(os.Stdout, version.Version)
	} else {
		RunServer()
	}
}
