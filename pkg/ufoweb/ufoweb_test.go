package ufoweb

import (
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/test/mocks/mock_ufoweb"
	"testing"
)

func TestOutputLoop(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockConn := mock_ufoweb.NewMockConn(ctrl)

	msg := &message.Output{}
	b, err := json.Marshal(msg)
	require.Nil(t, err)
	exit := make(chan bool, 1)

	mockConn.EXPECT().WriteMessage(websocket.TextMessage, b).
		Do(func(_, _ interface{}) { exit <- true })

	output := make(chan *message.Output, 1)
	output <- msg
	OutputLoop(mockConn, output, exit)
}

func TestWriteMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockConn := mock_ufoweb.NewMockConn(ctrl)

	output0 := &message.Output{}
	data0, err := json.Marshal(output0)
	require.Nil(t, err)
	mockConn.EXPECT().WriteMessage(websocket.TextMessage, data0)
	WriteMessage(mockConn, output0)

	output1 := &message.Output{Output: map[interface{}]int{}}
	_, expectedError := json.Marshal(output1)
	data1, err := json.Marshal(message.NewError(errors.Wrap(expectedError, "json.Marshal")))
	require.Nil(t, err)
	mockConn.EXPECT().WriteMessage(websocket.TextMessage, data1)
	WriteMessage(mockConn, output1)
}
