package readerstub

import (
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"reflect"
)

var _ reader.Output1D = &Output1D{}

type Output1D struct {
	Data []interface{}
}

func NewOutput1D(slice interface{}) *Output1D {
	v := reflect.ValueOf(slice)

	var length int
	if slice == nil {
		length = 0
	} else {
		length = v.Len()
	}

	data := make([]interface{}, length)
	for i := 0; i < length; i++ {
		data[i] = v.Index(i).Interface()
	}
	return &Output1D{data}
}

func (this *Output1D) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	fromTo := range1D.FromTo(len(this.Data))
	return TestResult1D(this.Data[fromTo.From:fromTo.To]), nil
}

func (this *Output1D) Shape() ([1]int, error) {
	return [1]int{len(this.Data)}, nil
}

var _ readresult.Result1D = TestResult1D{}

type TestResult1D []interface{}

func (this TestResult1D) Byte() []byte {
	result := make([]byte, len(this))
	for i := range this {
		result[i] = this[i].(byte)
	}
	return result
}

func (this TestResult1D) Interface() []interface{} {
	return this
}

func (this TestResult1D) Object() []map[string]interface{} {
	result := make([]map[string]interface{}, len(this))
	for i := range this {
		result[i] = this[i].(map[string]interface{})
	}
	return result
}
