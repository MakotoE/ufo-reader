package readerstub

import (
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"reflect"
)

var _ reader.Output2D = &Output2D{}

type Output2D struct {
	Slice2D interface{}
}

func (this *Output2D) Read(range2D reader.Range2D) (readresult.Result2D, error) {
	result, err := NewResult2D(this.Slice2D, range2D)
	return result, errors.Wrap(err, "NewResult2D")
}

func (this *Output2D) Shape() ([2]int, error) {
	if this.Slice2D == nil {
		return [2]int{}, nil
	}

	rows := reflect.ValueOf(this.Slice2D).Len()
	columns := reflect.ValueOf(this.Slice2D).Index(0).Len()
	return [2]int{rows, columns}, nil
}

var _ readresult.Result2D = Result2D{}

type Result2D struct {
	Slice2D interface{}
	Range2D reader.Range2D
}

func NewResult2D(slice2D interface{}, range2D reader.Range2D) (*Result2D, error) {
	return &Result2D{slice2D, range2D}, nil
}

func (this Result2D) Byte() [][]byte {
	if this.Slice2D == nil {
		return nil
	}

	constraint := reader.Range2D{
		reader.Coordinate2D{0, 0},
		reader.Area{uint(len(this.Slice2D.([][]byte))), uint(len(this.Slice2D.([][]byte)[0]))},
	}

	readRange := this.Range2D.Intersection(constraint)
	result := make([][]byte, readRange.Area.Rows)
	for row := 0; row < len(result); row++ {
		result[row] = make([]byte, readRange.Area.Cols)
		for col := 0; col < len(result[row]); col++ {
			slice := this.Slice2D.([][]byte)
			result[row][col] = slice[row+readRange.TopLeft.Row][col+readRange.TopLeft.Col]
		}
	}
	return result
}

func (this Result2D) Interface() [][]interface{} {
	if this.Slice2D == nil {
		return nil
	}

	constraint := reader.Range2D{
		reader.Coordinate2D{0, 0},
		reader.Area{
			uint(len(this.Slice2D.([][]interface{}))),
			uint(len(this.Slice2D.([][]interface{})[0])),
		},
	}
	readRange := this.Range2D.Intersection(constraint)
	result := make([][]interface{}, readRange.Area.Rows)
	for row := 0; row < len(result); row++ {
		result[row] = make([]interface{}, readRange.Area.Cols)
		for col := 0; col < len(result[row]); col++ {
			slice := this.Slice2D.([][]interface{})
			result[row][col] = slice[row+readRange.TopLeft.Row][col+readRange.TopLeft.Col]
		}
	}
	return result
}

func (this Result2D) Object() [][]map[string]interface{} {
	if this.Slice2D == nil {
		return nil
	}

	constraint := reader.Range2D{
		reader.Coordinate2D{0, 0},
		reader.Area{uint(len(this.Slice2D.([][]map[string]interface{}))), uint(len(this.Slice2D.([][]map[string]interface{})[0]))},
	}
	readRange := this.Range2D.Intersection(constraint)
	result := make([][]map[string]interface{}, readRange.Area.Rows)
	for row := 0; row < len(result); row++ {
		result[row] = make([]map[string]interface{}, readRange.Area.Cols)
		for col := 0; col < len(result[row]); col++ {
			slice := this.Slice2D.([][]map[string]interface{})
			result[row][col] = slice[row+readRange.TopLeft.Row][col+readRange.TopLeft.Col]
		}
	}
	return result
}
