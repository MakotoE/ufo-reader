package readerstub

import (
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var _ reader.Output0D = &Output0D{}

type Output0D struct {
	Data interface{}
}

func NewOutput0D(value interface{}) *Output0D {
	return &Output0D{value}
}

func (this *Output0D) Read() (readresult.Result0D, error) {
	return &Result0DStub{this.Data}, nil
}

var _ readresult.Result0D = &Result0DStub{}

type Result0DStub struct {
	Value interface{}
}

func (this *Result0DStub) Byte() byte {
	return this.Value.(byte)
}

func (this *Result0DStub) Interface() interface{} {
	return this.Value.(interface{})
}

func (this *Result0DStub) Object() map[string]interface{} {
	if this.Value == nil {
		return nil
	}
	return this.Value.(map[string]interface{})
}
