package averageobject

import (
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFrom1D = &AverageObject{}
	_ reader.Output0D   = &AverageObject{}
)

type AverageObject struct {
	ReadFrom reader.Output1D
}

func (this *AverageObject) CanRead(reader.Output1D) bool {
	return true
}

func (this *AverageObject) Init(_ map[string]interface{}, readFrom reader.Output1D) (reader.ReadFrom1D, error) {
	return &AverageObject{readFrom}, nil
}

func (this *AverageObject) Close() {
}

func (this *AverageObject) Read() (readresult.Result0D, error) {
	shape, err := this.ReadFrom.Shape()
	if err != nil {
		return nil, errors.Wrap(err, "Shape1D")
	}

	range1D, err := reader.NewRange1D(0, shape[0])
	if err != nil {
		return nil, errors.Wrap(err, "NewRange1D")
	}

	data, err := this.ReadFrom.Read(range1D)
	if err != nil {
		return nil, errors.Wrap(err, "ReadGeneric1D")
	}

	return Average(data.Object()), nil
}

func Average(a []map[string]interface{}) readresult.Object0D {
	var sum map[string]float64
	for _, entry := range a {
		sum = AddObject(sum, entry)
	}

	result := make(map[string]interface{})
	for k := range sum {
		result[k] = sum[k] / float64(len(a))
	}

	return result
}

func AddObject(a map[string]float64, b map[string]interface{}) map[string]float64 {
	if a == nil {
		a = make(map[string]float64)
	}

	for k := range b {
		a[k] += ConvertToFloat(b[k])
	}
	return a
}

func ConvertToFloat(a interface{}) float64 {
	switch a.(type) {
	case float64:
		return a.(float64)
	case byte:
		return float64(a.(byte))
	case int:
		return float64(a.(int))
	case uint32:
		return float64(a.(uint32))
	default:
		return 0
	}
}
