package averageobject_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/averageobject"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"testing"
)

func TestAverageObject_Read(t *testing.T) {
	stub := readerstub.NewOutput1D([]map[string]interface{}{{"a": 0}, {"a": 1}})

	r, err := (&averageobject.AverageObject{}).Init(nil, stub)
	require.Nil(t, err)

	result, err := r.(*averageobject.AverageObject).Read()
	assert.Equal(t, readresult.Object0D{"a": 0.5}, result)
}

func TestAverage(t *testing.T) {
	tests := []struct {
		a        []map[string]interface{}
		expected readresult.Object0D
	}{
		{
			nil,
			readresult.Object0D{},
		},
		{
			[]map[string]interface{}{{}},
			readresult.Object0D{},
		},
		{
			[]map[string]interface{}{{"a": 1}},
			readresult.Object0D{"a": 1.0},
		},
		{
			[]map[string]interface{}{{"a": 1}, {"a": 0}},
			readresult.Object0D{"a": 0.5},
		},
		{
			[]map[string]interface{}{{"a": 1}, nil},
			readresult.Object0D{"a": 0.5},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, averageobject.Average(test.a))
	}
}

func TestAddObject(t *testing.T) {
	tests := []struct {
		a        map[string]float64
		b        map[string]interface{}
		expected map[string]float64
	}{
		{
			nil,
			nil,
			map[string]float64{},
		},
		{
			map[string]float64{"a": 0},
			nil,
			map[string]float64{"a": 0},
		},
		{
			nil,
			map[string]interface{}{"a": 0},
			map[string]float64{"a": 0},
		},
		{
			map[string]float64{"a": 1},
			map[string]interface{}{"a": 2},
			map[string]float64{"a": 3},
		},
		{
			map[string]float64{"a": 1},
			map[string]interface{}{"a": 2.5},
			map[string]float64{"a": 3.5},
		},
		{
			map[string]float64{"a": 1},
			map[string]interface{}{"a": nil},
			map[string]float64{"a": 1},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, averageobject.AddObject(test.a, test.b))
	}
}
