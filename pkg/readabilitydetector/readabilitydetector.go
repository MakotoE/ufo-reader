package readabilitydetector

import (
	"gitlab.com/MakotoE/ufo-reader/pkg/expand0d"
	"gitlab.com/MakotoE/ufo-reader/pkg/index1d"
	"gitlab.com/MakotoE/ufo-reader/pkg/index2d"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readermap"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFromAny = &ReadablityDetector{}
	_ reader.Output1D    = &ReadablityDetector{}
)

type ReadablityDetector struct {
	ReadFrom reader.Reader
}

func (this *ReadablityDetector) CanRead(readFrom reader.Reader) bool {
	return true
}

func (this *ReadablityDetector) Init(options map[string]interface{}, readFrom reader.Reader) (reader.ReadFromAny, error) {
	return &ReadablityDetector{readFrom}, nil
}

func (this *ReadablityDetector) Close() {
}

func (this *ReadablityDetector) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	readable := ReadableReaders(this.ReadFrom)

	fromTo := range1D.FromTo(len(readable))
	return readresult.String1D(readable[fromTo.From:fromTo.To]), nil
}

func ReadableReaders(readFrom reader.Reader) []string {
	var result []string
	for name, readerStruct := range readermap.Map {
		if CanRead(readFrom, readerStruct) {
			result = append(result, name)
		}
	}
	return result
}

func CanRead(source reader.Reader, testingReader reader.Reader) bool {
	if readFromAny, ok := testingReader.(reader.ReadFromAny); ok {
		return readFromAny.CanRead(source)
	}
	if output0D, ok := source.(reader.Output0D); ok {
		if readFrom0D, ok := testingReader.(reader.ReadFrom0D); ok {
			return readFrom0D.CanRead(output0D)
		}
		if readFrom1D, ok := testingReader.(reader.ReadFrom1D); ok {
			return readFrom1D.CanRead(&expand0d.Expand0D{output0D})
		}
	}
	if output1D, ok := source.(reader.Output1D); ok {
		if readFrom0D, ok := testingReader.(reader.ReadFrom0D); ok {
			return readFrom0D.CanRead(&index1d.Index1D{output1D, 0})
		}
		if readFrom1D, ok := testingReader.(reader.ReadFrom1D); ok {
			return readFrom1D.CanRead(output1D)
		}
	} else if output2D, ok := source.(reader.Output2D); ok {
		if readFrom0D, ok := testingReader.(reader.ReadFrom0D); ok {
			return readFrom0D.CanRead(&index1d.Index1D{&index2d.Index2D{output2D, 0}, 0})
		}
		if readFrom1D, ok := testingReader.(reader.ReadFrom1D); ok {
			return readFrom1D.CanRead(&index2d.Index2D{output2D, 0})
		}
	}

	return false
}

func (this *ReadablityDetector) Shape() ([1]int, error) {
	return [1]int{len(ReadableReaders(this.ReadFrom))}, nil
}
