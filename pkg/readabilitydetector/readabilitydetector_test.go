package readabilitydetector_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/averageobject"
	"gitlab.com/MakotoE/ufo-reader/pkg/count"
	"gitlab.com/MakotoE/ufo-reader/pkg/csv"
	"gitlab.com/MakotoE/ufo-reader/pkg/expand0d"
	"gitlab.com/MakotoE/ufo-reader/pkg/file"
	"gitlab.com/MakotoE/ufo-reader/pkg/readabilitydetector"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/text"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestReadablityDetector_Read(t *testing.T) {
	tests := []struct {
		readFrom    reader.Reader
		range1D     reader.Range1D
		expectError bool
	}{
		{
			&text.Text{},
			reader.Range1D{0, 0},
			false,
		},
		{
			&text.Text{},
			reader.Range1D{0, 100},
			false,
		},
	}

	for _, test := range tests {
		// No need to test result
		_, err := (&readabilitydetector.ReadablityDetector{test.readFrom}).Read(test.range1D)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestReadableReaders(t *testing.T) {
	tests := []struct {
		readFrom         reader.Reader
		expectedContains []string
	}{
		{
			&text.Text{""},
			[]string{"Base64", "Count", "CSV"},
		},
	}

	for _, test := range tests {
		result := readabilitydetector.ReadableReaders(test.readFrom)
		for _, expected := range test.expectedContains {
			assert.Contains(t, result, expected)
		}
	}
}

func TestCanRead(t *testing.T) {
	var ( // Ensures the correct Reader types are being tested
		_ reader.Output1D     = &text.Text{}
		_ reader.ReadFromAny  = &readabilitydetector.ReadablityDetector{}
		_ reader.ReadFrom1D   = &csv.CSV{}
		_ reader.Output0D     = &averageobject.AverageObject{}
		_ reader.ReadFrom1D   = &count.Count{}
		_ reader.Output1D     = &count.Count{}
		_ reader.ReadFrom0D   = &expand0d.Expand0D{}
		_ reader.ReadFromNone = &file.File{}
	)

	tests := []struct {
		source        reader.Reader
		testingReader reader.Reader
		expected      bool
	}{
		{ // 1D -> ReadFromAny
			&text.Text{},
			&readabilitydetector.ReadablityDetector{},
			true,
		},
		{ // 1D -> 1D
			&text.Text{""},
			&csv.CSV{},
			true,
		},
		{
			&text.Text{"\""},
			&csv.CSV{},
			false,
		},
		{ // 0D -> 1D
			&averageobject.AverageObject{},
			&count.Count{},
			true,
		},
		{ // 1D -> 0D
			&count.Count{},
			&expand0d.Expand0D{},
			true,
		},
		{ // 1D -> ReadFromNone
			&text.Text{},
			&file.File{},
			false,
		},
	}

	for _, test := range tests {
		result := readabilitydetector.CanRead(test.source, test.testingReader)
		assert.Equal(t, test.expected, result)
	}
}
