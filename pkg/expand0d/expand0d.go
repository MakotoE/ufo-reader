package expand0d

import (
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFrom0D = &Expand0D{}
	_ reader.Output1D   = &Expand0D{}
)

type Expand0D struct {
	ReadFrom reader.Output0D
}

func (this *Expand0D) CanRead(reader.Output0D) bool {
	return true
}

func (this *Expand0D) Init(_ map[string]interface{}, readFrom reader.Output0D) (reader.ReadFrom0D, error) {
	return &Expand0D{readFrom}, nil
}

func (this *Expand0D) Close() {
}

func (this *Expand0D) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	value, err := this.ReadFrom.Read()
	if err != nil {
		return nil, errors.Wrap(err, "Read")
	}

	return NewResult(value, range1D), nil
}

func (this *Expand0D) Shape() ([1]int, error) {
	return [1]int{1}, nil
}
