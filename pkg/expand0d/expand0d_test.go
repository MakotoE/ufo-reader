package expand0d_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/expand0d"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"testing"
)

func TestExpand0D_Read(t *testing.T) {
	expand, err := (&expand0d.Expand0D{}).Init(nil, readerstub.NewOutput0D(byte(0)))
	require.Nil(t, err)
	result, err := expand.(*expand0d.Expand0D).Read(reader.Range1D{0, 1})
	assert.Equal(t, []byte{0}, result.Byte())
	assert.Nil(t, err)
}
