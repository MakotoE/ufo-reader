package expand0d

import (
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var _ readresult.Result1D = &Result{}

type Result struct {
	Data readresult.Result0D
}

func NewResult(data readresult.Result0D, range1D reader.Range1D) *Result {
	if range1D.From == 1 || range1D.Size == 0 {
		return &Result{nil}
	}

	return &Result{data}
}

func (this *Result) Byte() []byte {
	if this.Data == nil {
		return nil
	}
	return []byte{this.Data.Byte()}
}

func (this *Result) Interface() []interface{} {
	if this.Data == nil {
		return nil
	}
	return []interface{}{this.Data.Interface()}
}

func (this *Result) Object() []map[string]interface{} {
	if this.Data == nil {
		return nil
	}
	return []map[string]interface{}{this.Data.Object()}
}
