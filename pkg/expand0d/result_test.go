package expand0d_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/expand0d"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"testing"
)

func TestResult_Byte(t *testing.T) {
	tests := []struct {
		data     readresult.Result0D
		range1D  reader.Range1D
		expected []byte
	}{
		{
			nil,
			reader.Range1D{0, 1},
			nil,
		},
		{
			readresult.Object0D{},
			reader.Range1D{0, 0},
			nil,
		},
		{
			readresult.Object0D{},
			reader.Range1D{0, 1},
			[]byte{0},
		},
		{
			readresult.Object0D{},
			reader.Range1D{0, 2},
			[]byte{0},
		},
		{
			readresult.Object0D{},
			reader.Range1D{1, 1},
			nil,
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, expand0d.NewResult(test.data, test.range1D).Byte())
	}
}
