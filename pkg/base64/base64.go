package base64

import (
	"encoding/base64"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/ioreader"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"io"
	"io/ioutil"
)

var (
	_ reader.ReadFrom1D = &Base64{}
	_ reader.Output1D   = &Base64{}
)

type Base64 struct {
	ReadFrom reader.Output1D
}

func (this *Base64) CanRead(readFrom reader.Output1D) bool {
	_, err := (&Base64{readFrom}).Shape()
	return err == nil
}

func (this *Base64) Init(_ map[string]interface{}, readFrom reader.Output1D) (reader.ReadFrom1D, error) {
	return &Base64{readFrom}, nil
}

func (this *Base64) Close() {}

func (this *Base64) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	decoder := base64.NewDecoder(base64.StdEncoding, ioreader.New(this.ReadFrom))
	data := make([]byte, range1D.Size)
	n, err := decoder.Read(data)
	if err != nil && err != io.EOF {
		return nil, errors.Wrap(err, "Read")
	}

	fromTo := range1D.FromTo(n)
	return readresult.Byte1D(data[fromTo.From:fromTo.To]), nil
}

func (this *Base64) Shape() ([1]int, error) {
	data, err := ioutil.ReadAll(base64.NewDecoder(base64.StdEncoding, ioreader.New(this.ReadFrom)))
	if err != nil {
		return [1]int{}, errors.Wrap(err, "ReadAll")
	}
	return [1]int{len(data)}, nil
}

func (this *Base64) IOReader() io.Reader {
	return base64.NewDecoder(base64.StdEncoding, ioreader.New(this.ReadFrom))
}
