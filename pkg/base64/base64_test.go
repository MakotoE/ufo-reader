package base64_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/base64"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/pkg/text"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"io/ioutil"
	"testing"
)

func TestBase64_CanRead(t *testing.T) {
	tests := []struct {
		r        reader.Output1D
		expected bool
	}{
		{
			&text.Text{},
			true,
		},
		{
			&text.Text{"AAE="},
			true,
		},
		{
			&text.Text{"!"},
			false,
		},
	}

	for _, test := range tests {
		result := (&base64.Base64{}).CanRead(test.r)
		assert.Equal(t, test.expected, result)
	}
}

func TestBase64_Read(t *testing.T) {
	tests := []struct {
		str            string
		range1D        reader.Range1D
		expectedResult readresult.Result1D
		expectError    bool
	}{
		{
			"",
			reader.Range1D{0, 0},
			readresult.Byte1D{},
			false,
		},
		{
			"",
			reader.Range1D{0, 1},
			readresult.Byte1D{},
			false,
		},
		{
			"AAE=",
			reader.Range1D{0, 2},
			readresult.Byte1D{0, 1},
			false,
		},
		{
			"AAE=",
			reader.Range1D{1, 3},
			readresult.Byte1D{1},
			false,
		},
		{
			"A",
			reader.Range1D{0, 0},
			nil,
			true,
		},
	}

	for _, test := range tests {
		r, err := (&base64.Base64{}).Init(nil, &text.Text{test.str})
		assert.Nil(t, err)
		result, err := r.(*base64.Base64).Read(test.range1D)
		assert.Equal(t, test.expectedResult, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestBase64_Shape(t *testing.T) {
	r, err := (&base64.Base64{}).Init(nil, &text.Text{"AAE="})
	require.Nil(t, err)
	result, err := r.(*base64.Base64).Shape()
	assert.Equal(t, [1]int{2}, result)
	assert.Nil(t, err)
}

func TestBase64_IOReader(t *testing.T) {
	r, err := (&base64.Base64{}).Init(nil, &text.Text{"AAE="})
	result, _ := ioutil.ReadAll(r.(*base64.Base64).IOReader())
	assert.Equal(t, []byte{0, 1}, result)
	assert.Nil(t, err)
}
