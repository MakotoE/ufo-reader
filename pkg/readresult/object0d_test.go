package readresult_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"testing"
)

func TestObject0D_Byte(t *testing.T) {
	tests := []struct {
		readresult.Object0D
		expected byte
	}{
		{
			nil,
			0,
		},
		{
			readresult.Object0D{"a": 0},
			1,
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, test.Object0D.Byte())
	}
}

func TestObject0D_Interface(t *testing.T) {
	assert.Equal(t, map[string]interface{}{"a": 0}, readresult.Object0D{"a": 0}.Interface())
}
