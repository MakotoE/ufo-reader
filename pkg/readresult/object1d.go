package readresult

var _ Result1D = Object1D{}

type Object1D []map[string]interface{}

func (this Object1D) Byte() []byte {
	result := make([]byte, len(this))
	for i := range this {
		if len(this[i]) == 0 {
			result[i] = 0
		} else {
			result[i] = 1
		}
	}
	return result
}

func (this Object1D) Interface() []interface{} {
	result := make([]interface{}, len(this))
	for i := range this {
		result[i] = this[i]
	}
	return result
}

func (this Object1D) Object() []map[string]interface{} {
	return this
}
