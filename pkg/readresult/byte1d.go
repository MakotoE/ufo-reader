package readresult

var _ Result1D = Byte1D{}

type Byte1D []byte

func (this Byte1D) Byte() []byte {
	return this
}

func (this Byte1D) Interface() []interface{} {
	result := make([]interface{}, len(this))
	for i := range this {
		result[i] = this[i]
	}
	return result
}

func (this Byte1D) Object() []map[string]interface{} {
	result := make([]map[string]interface{}, len(this))
	for i := range this {
		result[i] = map[string]interface{}{"0": this[i]}
	}
	return result
}
