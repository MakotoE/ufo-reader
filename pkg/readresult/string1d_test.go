package readresult_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"testing"
)

func TestString1D_Byte(t *testing.T) {
	tests := []struct {
		readresult.String1D
		expected []byte
	}{
		{
			nil,
			[]byte{},
		},
		{
			readresult.String1D{"a"},
			[]byte{'a'},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, test.String1D.Byte())
	}
}

func TestString1D_Interface(t *testing.T) {
	tests := []struct {
		readresult.String1D
		expected []interface{}
	}{
		{
			nil,
			[]interface{}{},
		},
		{
			readresult.String1D{"a"},
			[]interface{}{"a"},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, test.String1D.Interface())
	}
}

func TestString1D_Object(t *testing.T) {
	tests := []struct {
		readresult.String1D
		expected []map[string]interface{}
	}{
		{
			nil,
			[]map[string]interface{}{},
		},
		{
			readresult.String1D{"a"},
			[]map[string]interface{}{{"0": "a"}},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, test.String1D.Object())
	}
}
