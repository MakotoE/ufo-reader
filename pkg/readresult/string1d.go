package readresult

var _ Result1D = String1D{}

type String1D []string

func (this String1D) Byte() []byte {
	result := make([]byte, len(this))
	for i := range this {
		result[i] = this[i][0]
	}
	return result
}

func (this String1D) Interface() []interface{} {
	result := make([]interface{}, len(this))
	for i := range this {
		result[i] = this[i]
	}
	return result
}

func (this String1D) Object() []map[string]interface{} {
	result := make([]map[string]interface{}, len(this))
	for i := range this {
		result[i] = map[string]interface{}{"0": this[i]}
	}
	return result
}
