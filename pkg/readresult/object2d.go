package readresult

var _ Result2D = Object2D{}

type Object2D [][]map[string]interface{}

func (this Object2D) Byte() [][]byte {
	// TODO should make a helper method to check column lengths
	result := make([][]byte, len(this))
	for row := range this {
		result[row] = make([]byte, len(this[row]))
		for column := range this[row] {
			if len(this[row][column]) == 0 {
				result[row][column] = 0
			} else {
				result[row][column] = 1
			}
		}
	}
	return result
}

func (this Object2D) Interface() [][]interface{} {
	result := make([][]interface{}, len(this))
	for row := range this {
		result[row] = make([]interface{}, len(this[row]))
		for column := range this[row] {
			result[row][column] = this[row][column]
		}
	}
	return result
}

func (this Object2D) Object() [][]map[string]interface{} {
	return this
}
