package readresult

var _ Result0D = Object0D{}

type Object0D map[string]interface{}

func (this Object0D) Byte() byte {
	if len(this) == 0 {
		return 0
	}
	return 1
}

func (this Object0D) Interface() interface{} {
	return map[string]interface{}(this)
}

func (this Object0D) Object() map[string]interface{} {
	return this
}
