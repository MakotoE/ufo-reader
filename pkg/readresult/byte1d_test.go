package readresult

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestByte1D_Interface(t *testing.T) {
	result := Byte1D([]byte{1}).Interface()
	assert.Equal(t, []interface{}{byte(1)}, result)
}

func TestByte1D_Object(t *testing.T) {
	result := Byte1D([]byte{1}).Object()
	assert.Equal(t, []map[string]interface{}{{"0": byte(1)}}, result)
}
