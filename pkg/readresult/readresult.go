package readresult

type Result0D interface {
	Byte() byte
	Interface() interface{}
	Object() map[string]interface{}
}

type Result1D interface {
	Byte() []byte
	Interface() []interface{}
	Object() []map[string]interface{}
}

// Result2D is a 2D matrix. The index notation is [row][column]. All row lengths are equal.
type Result2D interface {
	Byte() [][]byte
	Interface() [][]interface{}
	Object() [][]map[string]interface{}
}
