package readresult_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"testing"
)

func TestObject2D_Byte(t *testing.T) {
	tests := []struct {
		readresult.Object2D
		expected [][]byte
	}{
		{
			readresult.Object2D(nil),
			[][]byte{},
		},
		{
			readresult.Object2D{{}},
			[][]byte{{}},
		},
		{
			readresult.Object2D{{{}, {}}, {{"1": 1}}},
			[][]byte{{0, 0}, {1}},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, test.Object2D.Byte())
	}
}

func TestObject2D_Interface(t *testing.T) {
	result := readresult.Object2D{{{"a": 0}}}.Interface()
	assert.Equal(t, [][]interface{}{{map[string]interface{}{"a": 0}}}, result)
}
