package readresult_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"testing"
)

func TestObject1D_Byte(t *testing.T) {
	tests := []struct {
		object1D readresult.Object1D
		expected []byte
	}{
		{
			nil,
			[]byte{},
		},
		{
			readresult.Object1D{nil},
			[]byte{0},
		},
		{
			readresult.Object1D{map[string]interface{}{"a": 0}},
			[]byte{1},
		},
		{
			readresult.Object1D{nil, map[string]interface{}{"a": 0}},
			[]byte{0, 1},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, test.object1D.Byte())
	}
}

func TestObject1D_Interface(t *testing.T) {
	tests := []struct {
		object1D readresult.Object1D
		expected []interface{}
	}{
		{
			nil,
			[]interface{}{},
		},
		{
			readresult.Object1D{map[string]interface{}{"a": 0}},
			[]interface{}{map[string]interface{}{"a": 0}},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, test.object1D.Interface())
	}
}
