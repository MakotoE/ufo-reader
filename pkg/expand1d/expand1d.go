package expand1d

import (
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFrom1D = &Expand1D{}
	_ reader.Output2D   = &Expand1D{}
)

type Expand1D struct {
	ReadFrom reader.Output1D
}

func (this *Expand1D) CanRead(reader.Output1D) bool {
	return true
}

func (this *Expand1D) Init(_ map[string]interface{}, readFrom reader.Output1D) (reader.ReadFrom1D, error) {
	return &Expand1D{readFrom}, nil
}

func (this *Expand1D) Close() {
}

func (this *Expand1D) Read(range2D reader.Range2D) (readresult.Result2D, error) {
	result, err := NewResult(this.ReadFrom, range2D)
	return result, errors.Wrap(err, "NewResult")
}

func (this *Expand1D) Shape() ([2]int, error) {
	shape, err := this.ReadFrom.Shape()
	if err != nil {
		return [2]int{}, errors.Wrap(err, "Shape")
	}
	return [2]int{1, shape[0]}, nil
}
