package expand1d

import (
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var _ readresult.Result2D = &Result{}

type Result struct {
	Data readresult.Result1D
}

// NewResult returns a 2D result where data from readFrom becomes row 0 of the result, and
// cropped to range2D.
func NewResult(readFrom reader.Output1D, range2D reader.Range2D) (*Result, error) {
	if range2D.Area.Rows == 0 {
		return &Result{nil}, nil
	}

	var from uint
	if range2D.TopLeft.Col < 0 {
		from = 0
	} else {
		from = uint(range2D.TopLeft.Col)
	}

	data, err := readFrom.Read(reader.Range1D{from, range2D.Area.Cols})
	if err != nil {
		return nil, errors.Wrap(err, "Read")
	}
	return &Result{data}, nil
}

func (this *Result) Byte() [][]byte {
	if this.Data == nil {
		return [][]byte{}
	}
	return [][]byte{this.Data.Byte()}
}

func (this *Result) Interface() [][]interface{} {
	if this.Data == nil {
		return [][]interface{}{}
	}
	return [][]interface{}{this.Data.Interface()}
}

func (this *Result) Object() [][]map[string]interface{} {
	if this.Data == nil {
		return [][]map[string]interface{}{}
	}
	return [][]map[string]interface{}{this.Data.Object()}
}
