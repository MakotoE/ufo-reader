package expand1d_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/expand1d"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"testing"
)

func TestExpand1D_Shape(t *testing.T) {
	expand, err := (&expand1d.Expand1D{}).Init(nil, readerstub.NewOutput1D([]byte{}))
	require.Nil(t, err)
	result, err := expand.(*expand1d.Expand1D).Shape()
	assert.Equal(t, [2]int{1, 0}, result)
}
