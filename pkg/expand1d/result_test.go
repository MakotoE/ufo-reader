package expand1d_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/expand1d"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestResult_Byte(t *testing.T) {
	tests := []struct {
		readfrom    reader.Output1D
		range2D     reader.Range2D
		expected    [][]byte
		expectError bool
	}{
		{
			readerstub.NewOutput1D(nil),
			reader.Range2D{},
			[][]byte{},
			false,
		},
		{
			readerstub.NewOutput1D([]byte{0}),
			reader.Range2D{},
			[][]byte{},
			false,
		},
		{
			readerstub.NewOutput1D([]byte{0}),
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{1, 1}},
			[][]byte{{0}},
			false,
		},
		{
			readerstub.NewOutput1D([]byte{0, 1}),
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{1, 1}},
			[][]byte{{0}},
			false,
		},
		{
			readerstub.NewOutput1D([]byte{0, 1}),
			reader.Range2D{reader.Coordinate2D{0, 1}, reader.Area{1, 2}},
			[][]byte{{1}},
			false,
		},
		{
			readerstub.NewOutput1D(nil),
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{1, 1}},
			[][]byte{{}},
			false,
		},
		{
			readerstub.NewOutput1D([]byte{0}),
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 1}},
			[][]byte{},
			false,
		},
		{
			readerstub.NewOutput1D([]byte{0}),
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{1, 0}},
			[][]byte{{}},
			false,
		},
		{
			readerstub.NewOutput1D([]byte{}),
			reader.Range2D{reader.Coordinate2D{-1, 0}, reader.Area{0, 0}},
			[][]byte{},
			false,
		},
	}

	for _, test := range tests {
		r, err := expand1d.NewResult(test.readfrom, test.range2D)
		testutils.CheckError(t, test.expectError, err)
		if err == nil {
			assert.Equal(t, test.expected, r.Byte())
		}
	}
}
