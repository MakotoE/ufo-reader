package reshape2d

import (
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFrom2D = &Reshape2D{}
	_ reader.Output2D   = &Reshape2D{}
)

type Reshape2D struct {
	ReadFrom reader.Output2D
	NewShape NewShape
}

func (this *Reshape2D) CanRead(reader.Output2D) bool {
	return true
}

func (this *Reshape2D) Init(options map[string]interface{}, readFrom reader.Output2D) (reader.ReadFrom2D, error) {
	newShape := &NewShape{}
	if err := mapstructure.Decode(options, newShape); err != nil {
		return nil, errors.Wrap(err, "Decode")
	}

	return &Reshape2D{readFrom, *newShape}, nil
}

func (this *Reshape2D) Close() {
}

func (this *Reshape2D) Read(range2D reader.Range2D) (readresult.Result2D, error) {
	shape, err := this.ReadFrom.Shape()
	if err != nil {
		return nil, errors.Wrap(err, "Shape")
	}

	area, err := reader.NewArea(shape[0], shape[1])
	if err != nil {
		return nil, errors.Wrap(err, "NewArea")
	}

	data, err := this.ReadFrom.Read(reader.Range2D{reader.Coordinate2D{0, 0}, area})
	if err != nil {
		return nil, errors.Wrap(err, "Read")
	}

	result, err := NewResult(data, this.NewShape)
	return result, errors.Wrap(err, "NewResult")
}

func (this *Reshape2D) Shape() ([2]int, error) {
	shape, err := this.ReadFrom.Shape()
	if err != nil {
		return [2]int{}, errors.Wrap(err, "Shape")
	}

	area, err := this.NewShape.Area(uint(shape[0] * shape[1]))
	if err != nil {
		return [2]int{}, errors.Wrap(err, "Area")
	}

	return [2]int{int(area.Rows), int(area.Cols)}, nil
}
