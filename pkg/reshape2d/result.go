package reshape2d

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var _ readresult.Result2D = &Result{}

type Result struct {
	Data readresult.Result2D
	Area reader.Area
}

type NewShape struct {
	// Axis is the constrained axis.
	Axis       uint
	AxisLength uint
}

func (this *NewShape) Valid() bool {
	return this.Axis == 0 || this.Axis == 1
}

func (this *NewShape) Area(dataSize uint) (reader.Area, error) {
	if this.AxisLength == 0 && dataSize == 0 {
		return reader.Area{0, 0}, nil
	}

	if this.AxisLength == 0 || dataSize%this.AxisLength != 0 || this.AxisLength > dataSize {
		s := "result cannot be reshaped to " + fmt.Sprintf("%#v", this)
		return reader.Area{}, errors.New(s)
	}

	otherAxisLength := dataSize / this.AxisLength
	if this.Axis == 0 {
		return reader.Area{this.AxisLength, otherAxisLength}, nil
	}

	return reader.Area{otherAxisLength, this.AxisLength}, nil
}

func NewResult(data readresult.Result2D, newShape NewShape) (*Result, error) {
	// TODO range parameter
	if !newShape.Valid() {
		return nil, errors.New("newShape is out of range: " + fmt.Sprintf("%#v", newShape))
	}

	area, err := newShape.Area(Length(data.Interface()))
	if err != nil {
		return nil, errors.Wrap(err, "Size")
	}
	return &Result{data, area}, nil
}

func Length(data [][]interface{}) uint {
	result := len(data)
	if result != 0 {
		result = result * len(data[0])
	}
	return uint(result)
}

func (this *Result) Byte() [][]byte {
	data := this.Data.Byte()
	flattened := make([]byte, this.Area.Rows*this.Area.Cols)
	for i := range data {
		copy(flattened[i*len(data[0]):(i+1)*len(data[0])], data[i])
	}

	result := make([][]byte, this.Area.Rows)
	for i := range result {
		result[i] = flattened[uint(i)*this.Area.Cols : uint(i+1)*this.Area.Cols]
	}

	return result
}

func (this *Result) Interface() [][]interface{} {
	data := this.Data.Interface()
	flattened := make([]interface{}, this.Area.Rows*this.Area.Cols)
	for i := range data {
		copy(flattened[i*len(data[0]):(i+1)*len(data[0])], data[i])
	}

	result := make([][]interface{}, this.Area.Rows)
	for i := range result {
		result[i] = flattened[uint(i)*this.Area.Cols : uint(i+1)*this.Area.Cols]
	}

	return result
}

func (this *Result) Object() [][]map[string]interface{} {
	data := this.Data.Object()
	flattened := make([]map[string]interface{}, this.Area.Rows*this.Area.Cols)
	for i := range data {
		copy(flattened[i*len(data[0]):(i+1)*len(data[0])], data[i])
	}

	result := make([][]map[string]interface{}, this.Area.Rows)
	for i := range result {
		result[i] = flattened[uint(i)*this.Area.Cols : uint(i+1)*this.Area.Cols]
	}

	return result
}
