package reshape2d_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/file"
	"gitlab.com/MakotoE/ufo-reader/pkg/png"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/pkg/reshape2d"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestReshape2D_Read(t *testing.T) {
	f, err := (&file.File{}).Init(map[string]interface{}{"Path": testutils.Dir() + "/2x2.png"})
	require.Nil(t, err)
	image, err := (&png.PNG{}).Init(nil, f.(reader.Output1D))
	require.Nil(t, err)

	tests := []struct {
		options     map[string]interface{}
		readFrom    reader.Output2D
		range2D     reader.Range2D // not implemented
		expected    [][]byte
		expectError bool
	}{
		{
			map[string]interface{}{"Axis": 0, "AxisLength": 2},
			image.(reader.Output2D),
			reader.Range2D{},
			[][]byte{{1, 1}, {1, 1}},
			false,
		},
		{
			map[string]interface{}{"Axis": 0, "AxisLength": 3},
			image.(reader.Output2D),
			reader.Range2D{},
			nil,
			true,
		},
	}

	for _, test := range tests {
		r, err := (&reshape2d.Reshape2D{}).Init(test.options, test.readFrom)
		require.Nil(t, err)
		result, err := r.(*reshape2d.Reshape2D).Read(test.range2D)
		testutils.CheckError(t, test.expectError, err)
		if err == nil {
			assert.Equal(t, test.expected, result.Byte())
		}
	}
}

func TestReshape2D_Shape(t *testing.T) {
	tests := []struct {
		reshape  *reshape2d.Reshape2D
		expected [2]int
	}{
		{
			&reshape2d.Reshape2D{
				&readerstub.Output2D{nil},
				reshape2d.NewShape{0, 0},
			},
			[2]int{0, 0},
		},
		{
			&reshape2d.Reshape2D{
				&readerstub.Output2D{[][]interface{}{{0}}},
				reshape2d.NewShape{0, 1},
			},
			[2]int{1, 1},
		},
		{
			&reshape2d.Reshape2D{
				&readerstub.Output2D{[][]interface{}{{0, 0}}},
				reshape2d.NewShape{0, 1},
			},
			[2]int{1, 2},
		},
	}

	for _, test := range tests {
		result, err := test.reshape.Shape()
		assert.Equal(t, test.expected, result)
		assert.Nil(t, err)
	}
}
