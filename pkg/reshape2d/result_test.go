package reshape2d_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/reshape2d"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

type ReadResultStub [][]byte

func (this ReadResultStub) Byte() [][]byte {
	return this
}

func (this ReadResultStub) Interface() [][]interface{} {
	// Used only for getting length
	result := make([][]interface{}, len(this))
	if len(this) != 0 {
		result[0] = make([]interface{}, len(this[0]))
	}
	return result
}

func (this ReadResultStub) Object() [][]map[string]interface{} {
	panic("implement me")
}

func TestNewShape_Size(t *testing.T) {
	tests := []struct {
		newShape    reshape2d.NewShape
		length      uint
		expected    reader.Area
		expectError bool
	}{
		{
			reshape2d.NewShape{0, 0},
			0,
			reader.Area{0, 0},
			false,
		},
		{
			reshape2d.NewShape{1, 0},
			0,
			reader.Area{0, 0},
			false,
		},
		{
			reshape2d.NewShape{0, 1},
			1,
			reader.Area{1, 1},
			false,
		},
		{
			reshape2d.NewShape{0, 1},
			2,
			reader.Area{1, 2},
			false,
		},
		{
			reshape2d.NewShape{1, 1},
			2,
			reader.Area{2, 1},
			false,
		},
		{ // axis length 0, length 1
			reshape2d.NewShape{0, 0},
			1,
			reader.Area{},
			true,
		},
		{
			reshape2d.NewShape{1, 0},
			1,
			reader.Area{},
			true,
		},
		{ // axis length > length
			reshape2d.NewShape{0, 1},
			0,
			reader.Area{},
			true,
		},
		{
			reshape2d.NewShape{0, 2},
			1,
			reader.Area{},
			true,
		},
		{ // (length / axis length) % 1 != 0
			reshape2d.NewShape{0, 2},
			3,
			reader.Area{},
			true,
		},
	}

	for _, test := range tests {
		result, err := test.newShape.Area(test.length)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestLength(t *testing.T) {
	tests := []struct {
		data     [][]interface{}
		expected uint
	}{
		{
			nil,
			0,
		},
		{
			[][]interface{}{{}},
			0,
		},
		{
			[][]interface{}{{}, {}},
			0,
		},
		{
			[][]interface{}{{0}},
			1,
		},
		{
			[][]interface{}{{0}, {}},
			2,
		},
		{
			[][]interface{}{{0, 1}, {}},
			4,
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, reshape2d.Length(test.data))
	}
}

func TestResult_Byte(t *testing.T) {
	tests := []struct {
		data        ReadResultStub
		newShape    reshape2d.NewShape
		expected    [][]byte
		expectError bool
	}{
		{
			nil,
			reshape2d.NewShape{0, 0},
			[][]byte{},
			false,
		},
		{
			[][]byte{{0}},
			reshape2d.NewShape{0, 0},
			nil,
			true,
		},
		{
			[][]byte{{0}, {1}},
			reshape2d.NewShape{0, 1},
			[][]byte{{0, 1}},
			false,
		},
		{
			[][]byte{{0}, {1}},
			reshape2d.NewShape{1, 1},
			[][]byte{{0}, {1}},
			false,
		},
		{
			[][]byte{{0}, {1}},
			reshape2d.NewShape{0, 2},
			[][]byte{{0}, {1}},
			false,
		},
		{
			[][]byte{{0}, {1}},
			reshape2d.NewShape{1, 2},
			[][]byte{{0, 1}},
			false,
		},
		{
			[][]byte{{0, 1}, {2, 3}},
			reshape2d.NewShape{0, 2},
			[][]byte{{0, 1}, {2, 3}},
			false,
		},
		{
			[][]byte{{0, 1}, {2, 3}},
			reshape2d.NewShape{1, 4},
			[][]byte{{0, 1, 2, 3}},
			false,
		},
	}

	for _, test := range tests {
		r, err := reshape2d.NewResult(test.data, test.newShape)
		testutils.CheckError(t, test.expectError, err)
		if err == nil {
			assert.Equal(t, test.expected, r.Byte())
		}
	}
}
