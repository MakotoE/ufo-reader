package index2d_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/index2d"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestResult_Byte(t *testing.T) {
	tests := []struct {
		readfrom    reader.Output2D
		row         int
		expected    []byte
		expectError bool
	}{
		{
			&readerstub.Output2D{[][]byte{{}}},
			0,
			[]byte{},
			false,
		},
		{
			&readerstub.Output2D{[][]byte{{0}}},
			0,
			[]byte{0},
			false,
		},
		{
			&readerstub.Output2D{[][]byte{{0, 1}, {2, 3}}},
			1,
			[]byte{2, 3},
			false,
		},
		{
			&readerstub.Output2D{nil},
			0,
			nil,
			true,
		},
		{
			&readerstub.Output2D{[][]byte{{0}}},
			1,
			nil,
			true,
		},
	}

	for _, test := range tests {
		r, err := index2d.NewResult(test.readfrom, test.row)
		testutils.CheckError(t, test.expectError, err)
		if err == nil {
			assert.Equal(t, test.expected, r.Byte())
		}
	}
}
