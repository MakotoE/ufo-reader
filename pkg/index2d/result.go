package index2d

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var _ readresult.Result1D = &Result{}

type Result struct {
	Data readresult.Result2D
	Row  int
}

func NewResult(readFrom reader.Output2D, row int) (*Result, error) {
	shape, err := readFrom.Shape()
	if err != nil {
		return nil, errors.Wrap(err, "Shape")
	}

	area, err := reader.NewArea(1, shape[1])
	if err != nil {
		return nil, errors.Wrap(err, "NewArea")
	}

	data, err := readFrom.Read(reader.Range2D{reader.Coordinate2D{row, 0}, area})
	if err != nil {
		return nil, errors.Wrap(err, "Read")
	}

	if len(data.Byte()) == 0 {
		return nil, errors.New("row is out of range: " + fmt.Sprintf("%#v", row))
	}

	return &Result{data, row}, nil
}

func (this *Result) Byte() []byte {
	return this.Data.Byte()[0]
}

func (this *Result) Interface() []interface{} {
	return this.Data.Interface()[0]
}

func (this *Result) Object() []map[string]interface{} {
	return this.Data.Object()[0]
}
