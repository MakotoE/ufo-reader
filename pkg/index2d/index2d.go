package index2d

import (
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFrom2D = &Index2D{}
	_ reader.Output1D   = &Index2D{}
)

// Index2D selects a row from a 2D result by index.
type Index2D struct {
	ReadFrom reader.Output2D
	Row      int
}

func (this *Index2D) Close() {
}

func (this *Index2D) CanRead(reader.Output2D) bool {
	return true
}

func (this *Index2D) Init(options map[string]interface{}, readFrom reader.Output2D) (reader.ReadFrom2D, error) {
	o := &struct{ Row int }{}
	if err := mapstructure.Decode(options, o); err != nil {
		return nil, errors.Wrap(err, "Decode")
	}
	return &Index2D{readFrom, o.Row}, nil
}

func (this *Index2D) Read(reader.Range1D) (readresult.Result1D, error) {
	result, err := NewResult(this.ReadFrom, this.Row)
	return result, errors.Wrap(err, "NewResult")
}

func (this *Index2D) Shape() ([1]int, error) {
	shape, err := this.ReadFrom.Shape()
	return [1]int{shape[1]}, errors.Wrap(err, "Shape")
}
