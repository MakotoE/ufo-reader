package index2d_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/index2d"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"testing"
)

func TestIndex2D_Shape(t *testing.T) {
	index, err := (&index2d.Index2D{}).Init(nil, &readerstub.Output2D{[][]byte{{0, 1}}})
	require.Nil(t, err)

	result, err := index.(*index2d.Index2D).Shape()
	assert.Equal(t, [1]int{2}, result)
	assert.Nil(t, err)
}
