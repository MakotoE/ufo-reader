package count

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"sort"
)

var (
	_ reader.ReadFrom1D = &Count{}
	_ reader.Output1D   = &Count{}
)

type Count struct {
	ReadFrom reader.Output1D
}

func (this *Count) CanRead(reader.Output1D) bool {
	return true
}

func (this *Count) Init(_ map[string]interface{}, readFrom reader.Output1D) (reader.ReadFrom1D, error) {
	return &Count{readFrom}, nil
}

func (this *Count) Close() {}

func (this *Count) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	count, err := ValueCountResult(this.ReadFrom)
	if err != nil {
		return nil, errors.Wrap(err, "ValueCount")
	}

	fromTo := range1D.FromTo(len(count))
	return EntriesToResult(count[fromTo.From:fromTo.To]), nil
}

func (this *Count) Shape() ([1]int, error) {
	count, err := ValueCountResult(this.ReadFrom)
	if err != nil {
		return [1]int{}, errors.Wrap(err, "ValueCount")
	}

	return [1]int{len(count)}, errors.Wrap(err, "ValueCount")
}

func ValueCountResult(readFrom reader.Output1D) ([]Entry, error) {
	shape, err := readFrom.Shape()
	if err != nil {
		return nil, errors.Wrap(err, "Shape1D")
	}

	if shape[0] < 0 {
		return nil, errors.New("shape is out of range: " + fmt.Sprintf("%#v", shape))
	}

	values, err := readFrom.Read(reader.Range1D{0, uint(shape[0])})
	if err != nil {
		return nil, errors.Wrap(err, "ReadGeneric1D")
	}

	count, err := ValueCount(values.Interface())
	return count, errors.Wrap(err, "ValueCount")
}

func EntriesToResult(entries []Entry) readresult.Object1D {
	result := make(readresult.Object1D, len(entries))
	for i := range entries {
		result[i] = map[string]interface{}{"Value": entries[i].Key, "Count": entries[i].Value}
	}
	return result
}

type EntrySorter []Entry

func (this EntrySorter) Len() int {
	return len(this)
}

func (this EntrySorter) Less(i, j int) bool {
	return this[i].Value.(int) < this[j].Value.(int)
}

func (this EntrySorter) Swap(i, j int) {
	this[i], this[j] = this[j], this[i]
}

func ValueCount(values []interface{}) ([]Entry, error) {
	m := NewGenericMap()
	for _, value := range values {
		count := m.Get(value)
		if count == nil {
			count = 0
		}

		count = count.(int) + 1
		if err := m.Add(value, count); err != nil {
			return nil, errors.Wrap(err, "Add")
		}
	}

	entries := EntrySorter(m.Entries())
	sort.Sort(entries)

	return entries, nil
}
