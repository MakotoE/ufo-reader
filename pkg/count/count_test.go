package count_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/count"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestCount_Read(t *testing.T) {
	tests := []struct {
		readFrom    *readerstub.Output1D
		range1D     reader.Range1D
		expected    readresult.Result1D
		expectError bool
	}{
		{
			readerstub.NewOutput1D(nil),
			reader.Range1D{0, 0},
			readresult.Object1D{},
			false,
		},
		{
			readerstub.NewOutput1D([]int{0, 1}),
			reader.Range1D{0, 1},
			readresult.Object1D{{"Value": 0, "Count": 1}},
			false,
		},
		{
			readerstub.NewOutput1D([]int{}),
			reader.Range1D{0, 1},
			readresult.Object1D{},
			false,
		},
	}

	for _, test := range tests {
		result, err := (&count.Count{test.readFrom}).Read(test.range1D)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestCount_Shape(t *testing.T) {
	c := &count.Count{readerstub.NewOutput1D([]map[string]interface{}{{"a": "0"}})}
	result, err := c.Shape()
	assert.Equal(t, [1]int{1}, result)
	assert.Nil(t, err)
}

func TestEntriesToResult(t *testing.T) {
	tests := []struct {
		entries  []count.Entry
		expected readresult.Object1D
	}{
		{
			nil,
			readresult.Object1D{},
		},
		{
			[]count.Entry{{0, 1}},
			readresult.Object1D{{"Value": 0, "Count": 1}},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, count.EntriesToResult(test.entries))
	}
}

func TestValueCount(t *testing.T) {
	tests := []struct {
		values         []interface{}
		sortedExpected []count.Entry
	}{
		{
			nil,
			nil,
		},
		{
			[]interface{}{0},
			[]count.Entry{{0, 1}},
		},
		{
			[]interface{}{0, 0},
			[]count.Entry{{0, 2}},
		},
		{
			[]interface{}{0, 1, 1},
			[]count.Entry{{0, 1}, {1, 2}},
		},
		// 0 and nil should be different but has the same hash apparently. This should be fixed at
		// some point.
		{
			[]interface{}{0, "a", nil},
			[]count.Entry{{"a", 1}, {nil, 2}},
		},
		{
			[]interface{}{[]int{}},
			[]count.Entry{{[]int{}, 1}},
		},
	}

	for _, test := range tests {
		result, err := count.ValueCount(test.values)
		assert.Equal(t, test.sortedExpected, result)
		assert.Nil(t, err)
	}
}
