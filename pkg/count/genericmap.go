package count

import (
	"github.com/mitchellh/hashstructure"
	"github.com/pkg/errors"
)

type GenericMap struct {
	Keys map[uint64]interface{}
	Map  map[uint64]interface{}
}

func NewGenericMap() *GenericMap {
	return &GenericMap{make(map[uint64]interface{}), make(map[uint64]interface{})}
}

// Add associates value v to key k.
func (this *GenericMap) Add(k interface{}, v interface{}) error {
	hash, err := hashstructure.Hash(k, nil)
	if err != nil {
		return errors.Wrap(err, "Hash")
	}

	this.Keys[hash] = k
	this.Map[hash] = v
	return nil
}

// Get returns v associated with k. Returns nil if k doesn't exist.
func (this *GenericMap) Get(k interface{}) interface{} {
	hash, err := hashstructure.Hash(k, nil)
	if err != nil {
		return nil
	}

	return this.Map[hash]
}

type Entry struct {
	Key   interface{}
	Value interface{}
}

func (this *GenericMap) Entries() []Entry {
	var result []Entry
	for hash := range this.Keys {
		result = append(result, Entry{this.Keys[hash], this.Map[hash]})
	}
	return result
}
