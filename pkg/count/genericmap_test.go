package count_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/count"
	"testing"
)

func TestGenericMap_Add(t *testing.T) {
	tests := []struct {
		entries        []count.Entry
		expectedLength int
	}{
		{
			nil,
			0,
		},
		{
			[]count.Entry{{[]int{}, 0}},
			1,
		},
		{
			[]count.Entry{{[]int{}, 0}, {[]int{1}, 0}},
			2,
		},
		{
			[]count.Entry{{[]int{}, 0}, {[]int{}, 0}},
			1,
		},
		{
			[]count.Entry{{(*count.GenericMap)(nil), 0}, {(*testing.T)(nil), 0}},
			1,
		},
	}

	for _, test := range tests {
		m := count.NewGenericMap()
		for _, entry := range test.entries {
			err := m.Add(entry.Key, entry.Value)
			assert.Nil(t, err)
		}

		assert.Equal(t, test.expectedLength, len(m.Keys))
		assert.Equal(t, test.expectedLength, len(m.Map))
	}
}

func TestGenericMap_Get(t *testing.T) {
	type K struct{ n int }

	tests := []struct {
		add      []count.Entry
		k        interface{}
		expected interface{}
	}{
		{
			nil,
			0,
			nil,
		},
		{
			[]count.Entry{{"a", 0}},
			"a",
			0,
		},
		{
			[]count.Entry{{"a", 0}},
			nil,
			nil,
		},
		{
			[]count.Entry{{&K{0}, 0}},
			&K{0},
			0,
		},
		{
			[]count.Entry{{&K{0}, 0}},
			&K{1},
			0,
		},
	}

	for _, test := range tests {
		m := count.NewGenericMap()
		for _, entry := range test.add {
			err := m.Add(entry.Key, entry.Value)
			require.Nil(t, err)
		}

		result := m.Get(test.k)
		assert.Equal(t, test.expected, result)
	}
}

func TestGenericMap_Entries(t *testing.T) {
	tests := []struct {
		entries []count.Entry
	}{
		{nil},
		{[]count.Entry{{0, 1}}},
	}

	for _, test := range tests {
		m := count.NewGenericMap()
		for _, entry := range test.entries {
			err := m.Add(entry.Key, entry.Value)
			require.Nil(t, err)
		}

		assert.Equal(t, test.entries, m.Entries())
	}
}
