package session_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/chain"
	"gitlab.com/MakotoE/ufo-reader/pkg/logger"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/pkg/session"
	"gitlab.com/MakotoE/ufo-reader/pkg/text"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestChainArray_Get(t *testing.T) {
	tests := []struct {
		chainArray  session.ChainArray
		expected    *chain.Chain
		expectError bool
	}{
		{
			session.ChainArray{0: &chain.Chain{}},
			&chain.Chain{},
			false,
		},
		{
			nil,
			nil,
			true,
		},
	}

	for _, test := range tests {
		result, err := test.chainArray.Get(0)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestChainArray_GetOrAdd(t *testing.T) {
	tests := []struct {
		chainArray session.ChainArray
		expected   *chain.Chain
	}{
		{
			session.ChainArray{0: &chain.Chain{}},
			&chain.Chain{},
		},
		{
			session.ChainArray{},
			&chain.Chain{},
		},
	}

	for _, test := range tests {
		result := test.chainArray.GetOrAdd(0, nil)
		result.UnsubscribeChan = nil
		assert.Equal(t, test.expected, result)
	}
}

func TestSession_HandleVersion(t *testing.T) {
	output := make(chan *message.Output, 1)
	(&session.Session{OutputChan: output}).Version()
	expected := &message.Output{
		message.Message{"Output"},
		-1,
		struct{ Version string }{Version: "undefined"},
	}
	assert.Equal(t, expected, <-output)
}

func TestSession_HandleRead(t *testing.T) {
	output := make(chan *message.Output, 1)

	tests := []struct {
		session        *session.Session
		msg            *message.Read
		expectedOutput *message.Output
		expectError    bool
	}{
		{
			&session.Session{
				session.ChainArray{0: {Readers: chain.ReaderArray{&text.Text{"Str"}}, Output: output}},
				output,
				nil,
			},
			&message.Read{
				ChainID: 0,
				Index:   0,
				Range:   map[string]interface{}{"From": 0, "Size": 1},
			},
			&message.Output{message.Message{"Output"}, 0, readresult.Byte1D{'S'}},
			false,
		},
		{
			&session.Session{
				session.ChainArray{},
				output,
				nil,
			},
			&message.Read{
				ChainID: 0,
				Index:   0,
				Range:   map[string]interface{}{"From": 0, "Size": 1},
			},
			nil,
			true,
		},
	}

	for _, test := range tests {
		err := test.session.Read(test.msg)
		testutils.CheckError(t, test.expectError, err)
		select {
		case msg := <-output:
			assert.Equal(t, test.expectedOutput, msg)
		default:
			assert.Nil(t, test.expectedOutput)
		}
	}
}

func TestSession_HandleShape(t *testing.T) {
	output := make(chan *message.Output, 1)

	tests := []struct {
		chains         session.ChainArray
		msg            *message.Shape
		expectedOutput *message.Output
		expectError    bool
	}{
		{
			session.ChainArray{0: {Readers: chain.ReaderArray{&text.Text{"a"}}, Output: output}},
			&message.Shape{ChainID: 0, Index: 0},
			&message.Output{message.Message{"Output"}, 0, [1]int{1}},
			false,
		},
		{
			session.ChainArray{},
			&message.Shape{ChainID: 0, Index: 0},
			nil,
			true,
		},
	}

	for _, test := range tests {
		session := &session.Session{Chains: test.chains}
		err := session.Shape(test.msg)
		testutils.CheckError(t, test.expectError, err)
		select {
		case msg := <-output:
			assert.Equal(t, test.expectedOutput, msg)
		default:
			assert.Nil(t, test.expectedOutput)
		}
	}
}

func TestSession_Log(t *testing.T) {
	log := &logger.Logger{}
	log.Push(&message.Output{})
	output := make(chan *message.Output, 1)
	s := &session.Session{nil, output, log}
	err := s.Log()
	assert.Nil(t, err)

	select {
	case msg := <-output:
		_, ok := msg.Output.([]interface{})[0].(map[string]interface{})["ChainID"]
		assert.True(t, ok)
	default:
		assert.Fail(t, "did not receive output")
	}
}
