package session

import (
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/chain"
	"gitlab.com/MakotoE/ufo-reader/pkg/logger"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/version"
	"strconv"
)

type Session struct {
	Chains     ChainArray
	OutputChan chan<- *message.Output
	Logger     *logger.Logger
}

func (this *Session) Close() {
	for _, chain := range this.Chains {
		chain.Close()
	}
}

func (this *Session) RunBaseFunction(msg message.IMessage) error {
	switch msg.GetID() {
	case "Version":
		this.Version()
		return nil
	case "Read":
		return errors.Wrap(this.Read(msg.(*message.Read)), "Read")
	case "Shape":
		return errors.Wrap(this.Shape(msg.(*message.Shape)), "Shape")
	case "Subscribe":
		return errors.Wrap(this.Subscribe(msg.(*message.Subscribe)), "Subscribe")
	case "Unsubscribe":
		return errors.Wrap(this.Unsubscribe(msg.(*message.Unsubscribe)), "Unsubscribe")
	case "Log":
		return errors.Wrap(this.Log(), "Log")
	default:
		return errors.New("message type does not exist: " + msg.GetID())
	}
}

func (this *Session) Version() {
	this.OutputChan <- message.NewOutput(-1, struct{ Version string }{version.Version})
}

func (this *Session) Read(msg *message.Read) error {
	chain, err := this.Chains.Get(msg.ChainID)
	if err != nil {
		return errors.Wrap(err, "Get")
	}
	chain.Read(msg.Index, msg.Range)
	return nil
}

func (this *Session) Shape(msg *message.Shape) error {
	chain, err := this.Chains.Get(msg.ChainID)
	if err != nil {
		return errors.Wrap(err, "Get")
	}
	chain.Shape(msg.Index)
	return nil
}

func (this *Session) Subscribe(msg *message.Subscribe) error {
	chain, err := this.Chains.Get(msg.ChainID)
	if err != nil {
		return errors.Wrap(err, "Get")
	}
	chain.Subscribe(msg.Index, msg.Range)
	return nil
}

func (this *Session) Unsubscribe(msg *message.Unsubscribe) error {
	chain, err := this.Chains.Get(msg.ChainID)
	if err != nil {
		return errors.Wrap(err, "Get")
	}
	chain.Unsubscribe()
	return nil
}

func (this *Session) Log() error {
	log, err := this.Logger.GetLog()
	if err != nil {
		return errors.Wrap(err, "GetLog")
	}
	this.OutputChan <- message.NewOutput(-1, log)
	return nil
}

type ChainArray map[int]*chain.Chain

func (this ChainArray) Get(i int) (*chain.Chain, error) {
	if this[i] == nil {
		return nil, errors.New("index does not exist: " + strconv.Itoa(i))
	}
	return this[i], nil
}

func (this ChainArray) GetOrAdd(i int, output chan<- *message.Output) *chain.Chain {
	if this[i] == nil {
		this[i] = chain.New(i, output)
	}
	return this[i]
}
