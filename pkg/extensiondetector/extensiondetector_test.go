package extensiondetector_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/extensiondetector"
	"gitlab.com/MakotoE/ufo-reader/pkg/filestat"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestExtensionDetector_CanRead(t *testing.T) {
	tests := []struct {
		readFrom *readerstub.Output0D
		expected bool
	}{
		{
			readerstub.NewOutput0D(map[string]interface{}{"Name": ""}),
			true,
		},
		{
			readerstub.NewOutput0D(nil),
			false,
		},
	}

	for _, test := range tests {
		result := (&extensiondetector.ExtensionDetector{test.readFrom}).CanRead(test.readFrom)
		assert.Equal(t, test.expected, result)
	}
}

func TestExtensionDetector_Read(t *testing.T) {
	options := map[string]interface{}{"Path": testutils.Dir() + "/1x1.png"}
	f, err := (&filestat.FileStat{}).Init(options)
	require.Nil(t, err)

	detector, err := (&extensiondetector.ExtensionDetector{}).Init(nil, f.(reader.Output0D))
	require.Nil(t, err)

	result0, err0 := detector.(*extensiondetector.ExtensionDetector).Read(reader.Range1D{0, 1})
	assert.Equal(t, readresult.String1D{"PNG"}, result0)
	assert.Nil(t, err0)
}

func TestMatchingReaders(t *testing.T) {
	tests := []struct {
		readFrom         *readerstub.Output0D
		expectedContains []string
		expectError      bool
	}{
		{
			readerstub.NewOutput0D(map[string]interface{}{"Name": ""}),
			[]string{"File"},
			false,
		},
		{
			readerstub.NewOutput0D(map[string]interface{}{"Name": "csv"}),
			[]string{"File"},
			false,
		},
		{
			readerstub.NewOutput0D(map[string]interface{}{"Name": ".csv"}),
			[]string{"CSV"},
			false,
		},
		{
			readerstub.NewOutput0D(nil),
			nil,
			true,
		},
		{
			readerstub.NewOutput0D(map[string]interface{}{"Name": 0}),
			nil,
			true,
		},
	}

	for _, test := range tests {
		result, err := extensiondetector.MatchingReaders(test.readFrom)
		for _, expected := range test.expectedContains {
			assert.Contains(t, result, expected)
		}
		testutils.CheckError(t, test.expectError, err)
	}
}
