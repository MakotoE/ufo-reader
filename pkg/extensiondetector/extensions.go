package extensiondetector

var extensionMap = map[string][]string{
	"":     {"File"},
	".csv": {"CSV"},
	".png": {"PNG"},
}
