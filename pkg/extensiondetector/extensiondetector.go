package extensiondetector

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"path"
	"strings"
)

var (
	_ reader.ReadFrom0D = &ExtensionDetector{}
	_ reader.Output1D   = &ExtensionDetector{}
)

type ExtensionDetector struct {
	ReadFrom reader.Output0D
}

func (this *ExtensionDetector) CanRead(readFrom reader.Output0D) bool {
	_, err := (&ExtensionDetector{readFrom}).Shape()
	return err == nil
}

func (this *ExtensionDetector) Init(_ map[string]interface{}, readFrom reader.Output0D) (reader.ReadFrom0D, error) {
	return &ExtensionDetector{readFrom}, nil
}

func (this *ExtensionDetector) Close() {
}

func (this *ExtensionDetector) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	matches, err := MatchingReaders(this.ReadFrom)
	fromTo := range1D.FromTo(len(matches))
	return matches[fromTo.From:fromTo.To], errors.Wrap(err, "MatchingReaders")
}

func MatchingReaders(readFrom reader.Output0D) (readresult.String1D, error) {
	info, err := readFrom.Read()
	if err != nil {
		return nil, errors.Wrap(err, "Read")
	}

	nameStr, ok := info.Object()["Name"].(string)
	if !ok {
		return nil, errors.New("Name is not a string: " + fmt.Sprintf("%#v", info.Object()["Name"]))
	}

	readers := extensionMap[strings.ToLower(path.Ext(nameStr))]
	return readresult.String1D(readers), nil
}

func (this *ExtensionDetector) Shape() ([1]int, error) {
	readers, err := MatchingReaders(this.ReadFrom)
	return [1]int{len(readers)}, errors.Wrap(err, "MatchingReaders")
}
