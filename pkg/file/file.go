package file

import (
	"bufio"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"io"
	"os"
	"path"
)

var (
	_ reader.ReadFromNone = &File{}
	_ reader.Output1D     = &File{}
)

type File struct {
	File *os.File
}

func (this *File) Init(options map[string]interface{}) (reader.Reader, error) {
	o := &struct{ Path string }{}
	if err := mapstructure.Decode(options, o); err != nil {
		return nil, errors.Wrap(err, "Decode")
	}

	file, err := os.Open(o.Path)
	if err != nil {
		return nil, errors.Wrap(err, "Open")
	}

	info, err := file.Stat()
	if err != nil {
		return nil, errors.Wrap(err, "Stat")
	}
	if info.IsDir() {
		return nil, errors.New("Path must point to a file: " + o.Path)
	}

	return &File{file}, nil
}

func (this *File) Close() {
	_ = this.File.Close()
}

func (this *File) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	data := make(readresult.Byte1D, range1D.Size)

	resultSize, err := this.File.ReadAt(data, int64(range1D.From))
	if err != nil && errors.Cause(err) != io.EOF {
		return nil, errors.Wrap(err, "ReadAt")
	}

	return data[:resultSize], nil
}

func (this *File) Shape() ([1]int, error) {
	info, err := this.File.Stat()
	if err != nil {
		return [1]int{}, errors.Wrap(err, "Stat")
	}
	return [1]int{int(info.Size())}, nil
}

func (this *File) IOReader() io.Reader {
	_, _ = this.File.Seek(0, 0)
	return bufio.NewReader(this.File)
}

func (this *File) Extension() string {
	return path.Ext(this.File.Name())
}
