package file_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/file"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestFile_Init(t *testing.T) {
	tests := []struct {
		filename    string
		expectError bool
	}{
		{"empty", false},
		{"", true}, // directory
		{"thisFileDefinitelyDoesNotExist", true},
	}

	for _, test := range tests {
		options := map[string]interface{}{"Path": testutils.Dir() + "/" + test.filename}
		_, err := (&file.File{}).Init(options)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestFile_Close(t *testing.T) {
	r, _ := (&file.File{}).Init(map[string]interface{}{"Path": testutils.Dir() + "/empty"})
	r.Close()
	_, err := r.(*file.File).IOReader().Read([]byte{0})
	assert.NotNil(t, err)
}

func TestFile_Read(t *testing.T) {
	tests := []struct {
		filename       string
		range1D        reader.Range1D
		expectedResult readresult.Result1D
		expectError    bool
	}{
		{
			"empty",
			reader.Range1D{0, 0},
			readresult.Byte1D{},
			false,
		},
		{
			"1byte",
			reader.Range1D{0, 1},
			readresult.Byte1D{'a'},
			false,
		},
		{
			"1byte",
			reader.Range1D{0, 3},
			readresult.Byte1D{'a'},
			false,
		},
	}

	for _, test := range tests {
		func() {
			options := map[string]interface{}{"Path": testutils.Dir() + "/" + test.filename}
			r, err := (&file.File{}).Init(options)
			assert.Nil(t, err)
			defer r.Close()

			data, err := r.(*file.File).Read(test.range1D)
			assert.Equal(t, test.expectedResult, data)
			testutils.CheckError(t, test.expectError, err)
		}()
	}
}

func TestFile_Shape(t *testing.T) {
	r, _ := (&file.File{}).Init(map[string]interface{}{"Path": testutils.Dir() + "/1byte"})
	defer r.Close()

	result, err := r.(*file.File).Shape()
	assert.Equal(t, [1]int{1}, result)
	assert.Nil(t, err)
}

func TestFile_IOReader(t *testing.T) {
	r, err := (&file.File{}).Init(map[string]interface{}{"Path": testutils.Dir() + "/1byte"})
	require.Nil(t, err)
	defer r.Close()

	firstRead := make([]byte, 1)
	_, _ = r.(*file.File).IOReader().Read(firstRead)
	assert.Equal(t, []byte{'a'}, firstRead)

	secondRead := make([]byte, 1)
	_, _ = r.(*file.File).IOReader().Read(secondRead)
	assert.Equal(t, []byte{'a'}, secondRead)
}

func TestFile_Extension(t *testing.T) {
	tests := []struct {
		filename string
		expected string
	}{
		{"empty", ""},
		{"1x1.png", ".png"},
	}

	for _, test := range tests {
		func() {
			options := map[string]interface{}{"Path": testutils.Dir() + "/" + test.filename}
			r, err := (&file.File{}).Init(options)
			require.Nil(t, err)
			defer r.Close()
			assert.Equal(t, test.expected, r.(*file.File).Extension())
		}()
	}
}
