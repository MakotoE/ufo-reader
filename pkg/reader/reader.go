package reader

//go:generate mockgen -source=reader.go -destination=../../test/mocks/mock_reader/mock_reader.go

import (
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"io"
)

type Reader interface {
	Close() // Close must be thread safe
}

type ReadFromNone interface {
	Reader
	Init(options map[string]interface{}) (Reader, error)
}

type ReadFromAny interface {
	Reader
	// CanRead returns true if:
	// Given r = an instance of this Reader, initialized with passed Reader as the readFrom Reader
	// and with valid options;
	// Given range = widest valid Range;
	// r.Read(range) will not return an error which originates from this Reader.
	CanRead(Reader) bool
	Init(options map[string]interface{}, readFrom Reader) (ReadFromAny, error)
}

type ReadFrom0D interface {
	Reader
	CanRead(Output0D) bool
	Init(options map[string]interface{}, readFrom Output0D) (ReadFrom0D, error)
}

type ReadFrom1D interface {
	Reader
	CanRead(Output1D) bool
	Init(options map[string]interface{}, readFrom Output1D) (ReadFrom1D, error)
}

type ReadFrom2D interface {
	Reader
	CanRead(Output2D) bool
	Init(options map[string]interface{}, readFrom Output2D) (ReadFrom2D, error)
}

type Output0D interface {
	Read() (readresult.Result0D, error)
}

type Output1D interface {
	// Read returns data from Range1D.Low and to the minimum of Range1D.High and data source size.
	Read(Range1D) (readresult.Result1D, error)
	Shape() ([1]int, error) // Shape() will be used for the scroll bars
}

type Output2D interface {
	// Read returns data from Range2D.TopLeft and the minimum of Range2D.BottomRight and data source
	// size.
	Read(Range2D) (readresult.Result2D, error)
	Shape() ([2]int, error) // [rows][columns]
}

type Subscriber interface {
	Subscribe(update chan<- bool, unsubscribe <-chan bool)
}

type IOReader interface {
	IOReader() io.Reader
}
