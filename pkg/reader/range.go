package reader

import (
	"fmt"
	"github.com/pkg/errors"
)

// Range1D is a 1D range where both ends are greater than 0. Use FromTo() or ResultSize() to get
// valid end points.
type Range1D struct {
	// From is the lowest index of this range.
	From uint

	// Size = exclusive end index - From.
	Size uint
}

func NewRange1D(from int, size int) (Range1D, error) {
	if from < 0 || size < 0 {
		s := "from or size is out of range: " + fmt.Sprintf("%#v, %#v", from, size)
		return Range1D{}, errors.New(s)
	}

	return Range1D{uint(from), uint(size)}, nil
}

type FromTo struct {
	From int
	To   int
}

// FromTo returns the end points constrained to 0 and maxIndex.
func (this *Range1D) FromTo(maxIndex int) FromTo {
	return FromTo{
		Min(int(this.From), maxIndex),
		Min(int(this.From+this.Size), maxIndex),
	}
}

func Min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}

//type FromSize struct {
//	From int
//	Size int
//}
//
//// FromSize returns the low end point and length which are constrained to 0 and maxSize.
//func (this *Range1D) FromSize(maxSize int) FromSize {
//	return FromSize{
//		Min(this.From, uint(maxSize)),
//		Min(this.Size, uint(maxSize)),
//	}
//}

// Range2D is a 2D rectangular area.
type Range2D struct {
	// TopLeft is the top left most point.
	TopLeft Coordinate2D

	// Area = position of bottom right most point - position of top left most point.
	Area Area
}

// Coordinate2D is a 2D point.
type Coordinate2D struct {
	Row int
	Col int
}

// Area is a 2D vector.
type Area struct {
	Rows uint
	Cols uint
}

func NewArea(rows int, cols int) (Area, error) {
	if rows < 0 || cols < 0 {
		s := "rows or cols is out of range: " + fmt.Sprintf("%#v, %#v", rows, cols)
		return Area{}, errors.New(s)
	}
	return Area{uint(rows), uint(cols)}, nil
}

// Int returns an int version of Area.
func (this *Area) Int() struct {
	Rows int
	Cols int
} {
	return struct {
		Rows int
		Cols int
	}{int(this.Rows), int(this.Cols)}
}

func (this *Range2D) BottomRight() Coordinate2D {
	return Coordinate2D{
		this.TopLeft.Row + this.Area.Int().Rows,
		this.TopLeft.Col + this.Area.Int().Cols,
	}
}

func (this *Range2D) Intersection(a Range2D) Range2D {
	topLeft := Coordinate2D{
		Max(this.TopLeft.Row, a.TopLeft.Row),
		Max(this.TopLeft.Col, a.TopLeft.Col),
	}

	width := Min(this.BottomRight().Row, a.BottomRight().Row) - topLeft.Row
	height := Min(this.BottomRight().Col, a.BottomRight().Col) - topLeft.Col

	var area Area
	if width < 0 || height < 0 {
		area = Area{0, 0}
	} else {
		area = Area{uint(width), uint(height)}
	}

	return Range2D{topLeft, area}
}

func Max(a int, b int) int {
	if a < b {
		return b
	}
	return a
}
