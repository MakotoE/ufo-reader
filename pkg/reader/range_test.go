package reader_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestNewRange1D(t *testing.T) {
	tests := []struct {
		from        int
		size        int
		expected    reader.Range1D
		expectError bool
	}{
		{
			1,
			1,
			reader.Range1D{1, 1},
			false,
		},
		{
			-1,
			0,
			reader.Range1D{},
			true,
		},
		{
			0,
			-1,
			reader.Range1D{},
			true,
		},
	}

	for _, test := range tests {
		result, err := reader.NewRange1D(test.from, test.size)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestMin(t *testing.T) {
	tests := []struct {
		a        int
		b        int
		expected int
	}{
		{0, 1, 0},
		{1, 0, 0},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, reader.Min(test.a, test.b))
	}
}

func TestRange1D_FromTo(t *testing.T) {
	tests := []struct {
		range1D  reader.Range1D
		maxIndex int
		expected reader.FromTo
	}{
		{
			reader.Range1D{0, 0},
			0,
			reader.FromTo{0, 0},
		},
		{
			reader.Range1D{0, 1},
			1,
			reader.FromTo{0, 1},
		},
		{
			reader.Range1D{0, 1},
			0,
			reader.FromTo{0, 0},
		},
		{
			reader.Range1D{0, 3},
			1,
			reader.FromTo{0, 1},
		},
		{
			reader.Range1D{2, 3},
			1,
			reader.FromTo{1, 1},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, test.range1D.FromTo(test.maxIndex))
	}
}

func TestNewArea(t *testing.T) {
	tests := []struct {
		rows        int
		cols        int
		expected    reader.Area
		expectError bool
	}{
		{
			0,
			0,
			reader.Area{0, 0},
			false,
		},
		{
			-1,
			0,
			reader.Area{},
			true,
		},
		{
			0,
			-1,
			reader.Area{},
			true,
		},
	}

	for _, test := range tests {
		result, err := reader.NewArea(test.rows, test.cols)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestRange2D_Intersection(t *testing.T) {
	tests := []struct {
		range2D  reader.Range2D
		a        reader.Range2D
		expected reader.Range2D
	}{
		{
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 0}},
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 0}},
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 0}},
		},
		{
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{1, 1}},
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 0}},
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 0}},
		},
		{
			reader.Range2D{reader.Coordinate2D{1, 1}, reader.Area{0, 0}},
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 0}},
			reader.Range2D{reader.Coordinate2D{1, 1}, reader.Area{0, 0}},
		},
		{
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 0}},
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{1, 1}},
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 0}},
		},
		{
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{0, 0}},
			reader.Range2D{reader.Coordinate2D{1, 1}, reader.Area{0, 0}},
			reader.Range2D{reader.Coordinate2D{1, 1}, reader.Area{0, 0}},
		},
		{
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{1, 1}},
			reader.Range2D{reader.Coordinate2D{1, 1}, reader.Area{1, 1}},
			reader.Range2D{reader.Coordinate2D{1, 1}, reader.Area{0, 0}},
		},
		{
			reader.Range2D{reader.Coordinate2D{-1, -1}, reader.Area{2, 2}},
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{1, 1}},
			reader.Range2D{reader.Coordinate2D{0, 0}, reader.Area{1, 1}},
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, test.range2D.Intersection(test.a))
	}
}
