package readermap

import (
	"gitlab.com/MakotoE/ufo-reader/pkg/averageobject"
	"gitlab.com/MakotoE/ufo-reader/pkg/base64"
	"gitlab.com/MakotoE/ufo-reader/pkg/count"
	"gitlab.com/MakotoE/ufo-reader/pkg/csv"
	"gitlab.com/MakotoE/ufo-reader/pkg/currentdate"
	"gitlab.com/MakotoE/ufo-reader/pkg/expand0d"
	"gitlab.com/MakotoE/ufo-reader/pkg/expand1d"
	"gitlab.com/MakotoE/ufo-reader/pkg/extensiondetector"
	"gitlab.com/MakotoE/ufo-reader/pkg/file"
	"gitlab.com/MakotoE/ufo-reader/pkg/filestat"
	"gitlab.com/MakotoE/ufo-reader/pkg/index1d"
	"gitlab.com/MakotoE/ufo-reader/pkg/index2d"
	"gitlab.com/MakotoE/ufo-reader/pkg/magicnumberdetector"
	"gitlab.com/MakotoE/ufo-reader/pkg/mysql"
	"gitlab.com/MakotoE/ufo-reader/pkg/png"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/reshape2d"
	"gitlab.com/MakotoE/ufo-reader/pkg/selector"
	"gitlab.com/MakotoE/ufo-reader/pkg/text"
)

var Map = map[string]reader.Reader{
	"AverageObject":       &averageobject.AverageObject{},
	"Base64":              &base64.Base64{},
	"CurrentDate":         &currentdate.CurrentDate{},
	"Count":               &count.Count{},
	"CSV":                 &csv.CSV{},
	"Expand0D":            &expand0d.Expand0D{},
	"Expand1D":            &expand1d.Expand1D{},
	"ExtensionDetector":   &extensiondetector.ExtensionDetector{},
	"File":                &file.File{},
	"FileStat":            &filestat.FileStat{},
	"Index1D":             &index1d.Index1D{},
	"Index2D":             &index2d.Index2D{},
	"MagicNumberDetector": &magicnumberdetector.MagicNumberDetector{},
	"MySQL":               &mysql.MySQL{},
	"PNG":                 &png.PNG{},
	"Reshape2D":           &reshape2d.Reshape2D{},
	"Selector":            &selector.Selector{},
	"Text":                &text.Text{},
}

// TODO create Panic reader for debugging
