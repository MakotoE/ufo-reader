package currentdate

import (
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"time"
)

var (
	_ reader.ReadFromNone = &CurrentDate{}
	_ reader.Output0D     = &CurrentDate{}
)

type CurrentDate struct{}

func (this *CurrentDate) Init(map[string]interface{}) (reader.Reader, error) {
	return &CurrentDate{}, nil
}

func (this *CurrentDate) Close() {
}

func (this *CurrentDate) Read() (readresult.Result0D, error) {
	return GetCurrentDate(time.Now()), nil
}

func GetCurrentDate(t time.Time) readresult.Object0D {
	year, month, day := t.Date()
	hour, minute, second := t.Clock()
	return readresult.Object0D{
		"Year":   year,
		"Month":  int(month),
		"Day":    day,
		"Hour":   hour,
		"Minute": minute,
		"Second": second,
	}
}

func (this *CurrentDate) Subscribe(update chan<- bool, unsubscribe <-chan bool) {
	go Ticker(update, unsubscribe)
}

func Ticker(update chan<- bool, unsubscribe <-chan bool) {
	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-ticker.C:
			update <- true
		case <-unsubscribe:
			ticker.Stop()
			return
		}
	}
}
