package currentdate

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"testing"
	"time"
)

func TestGetCurrentDate(t *testing.T) {
	date := time.Date(0, 1, 2, 3, 4, 5, 0, time.UTC)
	expected := readresult.Object0D{
		"Year":   0,
		"Month":  1,
		"Day":    2,
		"Hour":   3,
		"Minute": 4,
		"Second": 5,
	}
	assert.Equal(t, expected, GetCurrentDate(date))
}

func TestTicker(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	update := make(chan bool, 1)
	unsubscribe := make(chan bool, 1)
	ticks := 0

	go func() {
		for {
			select {
			case <-update:
				ticks++
			}
		}
	}()

	go Ticker(update, unsubscribe)
	time.Sleep(time.Millisecond * 3900) // Wait for 3 ticks
	unsubscribe <- true
	time.Sleep(time.Millisecond * 1100) // Sleep to check if unsubscribed
	assert.Equal(t, 3, ticks)
}
