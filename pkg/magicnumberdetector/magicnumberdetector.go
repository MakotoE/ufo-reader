package magicnumberdetector

import (
	"crypto/sha256"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFrom1D = &MagicNumberDetector{}
	_ reader.Output1D   = &MagicNumberDetector{}
)

type MagicNumberDetector struct {
	ReadFrom reader.Output1D
}

func (this *MagicNumberDetector) CanRead(reader.Output1D) bool {
	return true
}

func (this *MagicNumberDetector) Init(_ map[string]interface{}, readFrom reader.Output1D) (reader.ReadFrom1D, error) {
	return &MagicNumberDetector{readFrom}, nil
}

func (this *MagicNumberDetector) Close() {
}

func (this *MagicNumberDetector) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	matches, err := MatchingReaders(this.ReadFrom)
	fromTo := range1D.FromTo(len(matches))
	return readresult.String1D(matches)[fromTo.From:fromTo.To], errors.Wrap(err, "MatchingReaders")
}

func MatchingReaders(readFrom reader.Output1D) ([]string, error) {
	var result []string
	for i := 0; i < maxMagicNumberLength; i++ {
		matches, err := CheckBytes(readFrom, i)
		if err != nil {
			return nil, errors.Wrap(err, "CheckBytes")
		}
		result = append(result, matches...)
	}
	return result, nil
}

func CheckBytes(readFrom reader.Output1D, index int) ([]string, error) {
	shape, err := readFrom.Shape()
	if err != nil {
		return nil, errors.Wrap(err, "Shape1D")
	}
	if index > shape[0]-1 { // End of file
		return nil, nil
	}

	data, err := readFrom.Read(reader.Range1D{0, uint(index + 1)})
	if err != nil {
		return nil, errors.Wrap(err, "ReadByte1D")
	}

	return magicNumberMap[sha256.Sum256(data.Byte())], nil
}

func (this *MagicNumberDetector) Shape() ([1]int, error) {
	matching, err := MatchingReaders(this.ReadFrom)
	return [1]int{len(matching)}, errors.Wrap(err, "MatchingReaders")
}
