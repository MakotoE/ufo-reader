package magicnumberdetector_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/file"
	"gitlab.com/MakotoE/ufo-reader/pkg/magicnumberdetector"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestMagicNumberDetector_Read(t *testing.T) {
	tests := []struct {
		readFrom    reader.Output1D
		range1D     reader.Range1D
		expected    readresult.Result1D
		expectError bool
	}{
		{
			readerstub.NewOutput1D(nil),
			reader.Range1D{},
			readresult.String1D(nil),
			false,
		},
		{
			readerstub.NewOutput1D(nil),
			reader.Range1D{0, 1},
			readresult.String1D(nil),
			false,
		},
	}

	for _, test := range tests {
		m := &magicnumberdetector.MagicNumberDetector{test.readFrom}
		result, err := m.Read(test.range1D)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestMatchingReaders(t *testing.T) {
	f, err := (&file.File{}).Init(map[string]interface{}{"Path": testutils.Dir() + "/1x1.png"})
	require.Nil(t, err)

	tests := []struct {
		readFrom reader.Output1D
		expected []string
	}{
		{
			readerstub.NewOutput1D(nil),
			nil,
		},
		{
			f.(*file.File),
			[]string{"PNG"},
		},
	}

	for _, test := range tests {
		result, err := magicnumberdetector.MatchingReaders(test.readFrom)
		assert.Equal(t, test.expected, result)
		assert.Nil(t, err)
	}
}

func TestCheckBytes(t *testing.T) {
	pngMagicNumber := func() []byte {
		b := []byte{0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a}
		result := make([]byte, len(b))
		copy(result, b)
		return result
	}

	tests := []struct {
		readFrom   reader.Output1D
		index      int
		expected   []string
		expectEror bool
	}{
		{
			readerstub.NewOutput1D(nil),
			0,
			nil,
			false,
		},
		{
			readerstub.NewOutput1D(nil),
			1,
			nil,
			false,
		},
		{
			readerstub.NewOutput1D(pngMagicNumber()),
			0,
			nil,
			false,
		},
		{
			readerstub.NewOutput1D(pngMagicNumber()),
			len(pngMagicNumber()) - 1,
			[]string{"PNG"},
			false,
		},
		{
			readerstub.NewOutput1D(pngMagicNumber()),
			len(pngMagicNumber()),
			nil,
			false,
		},
	}

	for _, test := range tests {
		result, err := magicnumberdetector.CheckBytes(test.readFrom, test.index)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectEror, err)
	}
}
