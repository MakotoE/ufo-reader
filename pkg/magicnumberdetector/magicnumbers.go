package magicnumberdetector

import "crypto/sha256"

var magicNumbers = []MagicNumberAndReader{
	{[]byte{0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a}, "PNG"},
}

var (
	magicNumberMap       = GenerateMagicNumberMap(magicNumbers)
	maxMagicNumberLength = FindMaxMagicNumberLength(magicNumbers)
)

type MagicNumberAndReader struct {
	MagicNumber []byte
	Reader      string
}

func GenerateMagicNumberMap(magicNumbers []MagicNumberAndReader) map[[sha256.Size]byte][]string {
	magicNumberMap := map[[sha256.Size]byte][]string{}

	for _, magicNumber := range magicNumbers {
		hash := sha256.Sum256(magicNumber.MagicNumber)
		magicNumberMap[hash] = append(magicNumberMap[hash], magicNumber.Reader)
	}
	return magicNumberMap
}

func FindMaxMagicNumberLength(magicNumbers []MagicNumberAndReader) int {
	maxLength := 0
	for _, magicNumber := range magicNumbers {
		if len(magicNumber.MagicNumber) > maxLength {
			maxLength = len(magicNumber.MagicNumber)
		}
	}
	return maxLength
}
