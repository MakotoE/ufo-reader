package filestat

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"os"
	"testing"
	"time"
)

type FileInfoStub int

func (FileInfoStub) Name() string {
	return "name"
}

func (FileInfoStub) Size() int64 {
	return 1
}

func (FileInfoStub) Mode() os.FileMode {
	return os.ModeDir
}

func (FileInfoStub) ModTime() time.Time {
	return time.Date(0, 0, 0, 0, 0, 0, 0, time.UTC)
}

func (FileInfoStub) IsDir() bool {
	return true
}

func (FileInfoStub) Sys() interface{} {
	panic("not implemented")
}

func TestMap(t *testing.T) {
	result := Map(FileInfoStub(0))
	expected := readresult.Object0D{
		"Name":              "name",
		"Size":              int64(1),
		"Mode":              "d---------",
		"Modification time": "-0001-11-30 00:00:00 +0000 UTC",
		"Is directory":      true,
	}
	assert.Equal(t, expected, result)
}
