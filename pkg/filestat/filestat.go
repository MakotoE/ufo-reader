package filestat

import (
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"os"
)

var (
	_ reader.ReadFromNone = &FileStat{}
	_ reader.Output0D     = &FileStat{}
)

type FileStat struct {
	File *os.File
}

func (this *FileStat) Init(options map[string]interface{}) (reader.Reader, error) {
	o := &struct{ Path string }{}
	if err := mapstructure.Decode(options, o); err != nil {
		return nil, errors.Wrap(err, "Decode")
	}

	file, err := os.Open(o.Path)
	if err != nil {
		return nil, errors.Wrap(err, "Open")
	}
	return &FileStat{file}, nil
}

func (this *FileStat) Close() {
	this.File.Close()
}

func (this *FileStat) Read() (readresult.Result0D, error) {
	info, err := this.File.Stat()
	if err != nil {
		return nil, errors.Wrap(err, "Stat")
	}
	return Map(info), nil
}

// Map returns info as an object if info is not nil.
func Map(info os.FileInfo) readresult.Result0D {
	return readresult.Object0D{
		"Name":              info.Name(),
		"Size":              info.Size(),
		"Mode":              info.Mode().String(),
		"Modification time": info.ModTime().String(),
		"Is directory":      info.IsDir(),
	}
}
