package index1d

import (
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFrom1D = &Index1D{}
	_ reader.Output0D   = &Index1D{}
)

type Index1D struct {
	ReadFrom reader.Output1D
	Index    uint
}

func (this *Index1D) CanRead(reader.Output1D) bool {
	return true
}

func (this *Index1D) Init(options map[string]interface{}, readFrom reader.Output1D) (reader.ReadFrom1D, error) {
	o := &struct{ Index uint }{}
	if err := mapstructure.Decode(options, o); err != nil {
		return nil, errors.Wrap(err, "Decode")
	}
	return &Index1D{readFrom, o.Index}, nil
}

func (this *Index1D) Close() {
}

func (this *Index1D) Read() (readresult.Result0D, error) {
	result, err := NewResult(this.ReadFrom, this.Index)
	return result, errors.Wrap(err, "NewResult")
}
