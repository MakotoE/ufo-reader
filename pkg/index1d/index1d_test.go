package index1d_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/index1d"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"testing"
)

func TestIndex1D_Read(t *testing.T) {
	stub := readerstub.NewOutput1D([]byte{0, 1})
	index, err := (&index1d.Index1D{}).Init(map[string]interface{}{"Index": 1}, stub)
	require.Nil(t, err)

	result, err := index.(*index1d.Index1D).Read()
	assert.Equal(t, byte(1), result.Byte())
	assert.Nil(t, err)
}
