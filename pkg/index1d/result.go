package index1d

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var _ readresult.Result0D = &Result{}

type Result struct {
	Data  readresult.Result1D
	Index uint
}

func NewResult(readFrom reader.Output1D, index uint) (*Result, error) {
	data, err := readFrom.Read(reader.Range1D{index, index + 1})
	if err != nil {
		return nil, errors.Wrap(err, "Read")
	}

	if len(data.Byte()) == 0 {
		return nil, errors.New("index is out of range: " + fmt.Sprintf("%#v", index))
	}
	return &Result{data, index}, nil
}

func (this *Result) Byte() byte {
	return this.Data.Byte()[0]
}

func (this *Result) Interface() interface{} {
	return this.Data.Interface()[0]
}

func (this *Result) Object() map[string]interface{} {
	return this.Data.Object()[0]
}
