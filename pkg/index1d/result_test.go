package index1d_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/index1d"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestResult_Byte(t *testing.T) {
	tests := []struct {
		readFrom    reader.Output1D
		index       uint
		expected    byte
		expectError bool
	}{
		{
			readerstub.NewOutput1D([]byte{0, 1}),
			0,
			0,
			false,
		},
		{
			readerstub.NewOutput1D([]byte{0, 1}),
			1,
			1,
			false,
		},
		{
			readerstub.NewOutput1D(nil),
			0,
			0,
			true,
		},
		{
			readerstub.NewOutput1D([]byte{0, 1}),
			2,
			0,
			true,
		},
	}

	for _, test := range tests {
		r, err := index1d.NewResult(test.readFrom, test.index)
		testutils.CheckError(t, test.expectError, err)
		if err == nil {
			assert.Equal(t, test.expected, r.Byte())
		}
	}
}
