package csv_test

import (
	encodingCsv "encoding/csv"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/csv"
	"gitlab.com/MakotoE/ufo-reader/pkg/ioreader"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/pkg/text"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"io"
	"testing"
)

func TestCSV_CanRead(t *testing.T) {
	tests := []struct {
		r        reader.Output1D
		expected bool
	}{
		{&text.Text{}, true},
		{&text.Text{"\""}, false},
	}

	for _, test := range tests {
		result := (&csv.CSV{}).CanRead(test.r)
		assert.Equal(t, test.expected, result)
	}
}

func TestCSV_Read(t *testing.T) {
	tests := []struct {
		str            string
		containsHeader bool
		range1D        reader.Range1D
		expectedResult readresult.Result1D
		expectError    bool
	}{
		{
			"",
			false,
			reader.Range1D{0, 0},
			readresult.Object1D(nil),
			false,
		},
		{
			"",
			true,
			reader.Range1D{0, 0},
			readresult.Object1D(nil),
			false,
		},
		{
			"",
			false,
			reader.Range1D{0, 1},
			readresult.Object1D(nil),
			false,
		},
		{
			"a",
			false,
			reader.Range1D{0, 0},
			readresult.Object1D(nil),
			false,
		},
		{
			"a",
			true,
			reader.Range1D{0, 0},
			readresult.Object1D(nil),
			false,
		},
		{
			"a",
			false,
			reader.Range1D{0, 1},
			readresult.Object1D{{"0": "a"}},
			false,
		},
		{
			"a",
			true,
			reader.Range1D{0, 1},
			readresult.Object1D(nil),
			false,
		},
		{
			"0a\n1a,1b\n",
			false,
			reader.Range1D{0, 2},
			readresult.Object1D{{"0": "0a"}, {"0": "1a", "1": "1b"}},
			false,
		},
		{
			"0a\n1a,1b\n",
			true,
			reader.Range1D{0, 1},
			readresult.Object1D{{"0a": "1a", "1": "1b"}},
			false,
		},
		{
			"0a\n1a,1b\n",
			false,
			reader.Range1D{1, 2},
			readresult.Object1D{{"0": "1a", "1": "1b"}},
			false,
		},
		{
			"0a\n1a,1b\n",
			true,
			reader.Range1D{0, 1},
			readresult.Object1D{{"0a": "1a", "1": "1b"}},
			false,
		},
		{
			"0a\n1a,1b",
			false,
			reader.Range1D{0, 2},
			readresult.Object1D{{"0": "0a"}, {"0": "1a", "1": "1b"}},
			false,
		},
	}

	for _, test := range tests {
		options := map[string]interface{}{"ContainsHeader": test.containsHeader}
		r, err := (&csv.CSV{}).Init(options, readerstub.NewOutput1D(test.str))
		require.Nil(t, err)

		result, err := r.(*csv.CSV).Read(test.range1D)
		assert.Equal(t, test.expectedResult, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestNames(t *testing.T) {
	tests := []struct {
		str            string
		containsHeader bool
		expectedResult []string
	}{
		{"", false, nil},
		{"", true, nil},
		{"a", false, nil},
		{"a", true, []string{"a"}},
	}

	for _, test := range tests {
		result, err := csv.Names(ioreader.New(readerstub.NewOutput1D(test.str)), test.containsHeader)
		assert.Equal(t, test.expectedResult, result)
		assert.Nil(t, err)
	}
}

func TestSkipRows(t *testing.T) {
	tests := []struct {
		str             string
		n               uint
		containsHeader  bool
		expectedNextRow []string
	}{
		{"a", 0, false, []string{"a"}},
		{"a", 0, true, nil},
		{"a", 1, false, nil},
		{"a", 1, true, nil},
	}

	for _, test := range tests {
		csvReader := encodingCsv.NewReader(ioreader.New(readerstub.NewOutput1D(test.str)))
		csv.SkipRows(csvReader, test.n, test.containsHeader)
		nextRow, _ := csvReader.Read()
		assert.Equal(t, test.expectedNextRow, nextRow)
	}
}

func TestHeaderAt(t *testing.T) {
	tests := []struct {
		names    []string
		x        int
		expected string
	}{
		{[]string{"a"}, 0, "a"},
		{[]string{"a"}, 1, "1"},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, csv.HeaderAt(test.names, test.x))
	}
}

func TestNextRecord(t *testing.T) {
	tests := []struct {
		str            string
		names          []string
		expectedResult map[string]interface{}
		expectedError  error
	}{
		{"a", []string{"name"}, map[string]interface{}{"name": "a"}, nil},
		{"a", []string{}, map[string]interface{}{"0": "a"}, nil},
		{"", []string{}, nil, io.EOF},
	}

	for _, test := range tests {
		csvReader := encodingCsv.NewReader(ioreader.New(readerstub.NewOutput1D(test.str)))
		result, err := csv.NextRecord(csvReader, test.names)
		assert.Equal(t, test.expectedResult, result)
		assert.Equal(t, test.expectedError, errors.Cause(err))
	}
}

func TestCSV_Shape(t *testing.T) {
	tests := []struct {
		str            string
		containsHeader bool
		expectedShape  [1]int
		expectError    bool
	}{
		{"", false, [1]int{0}, false},
		{"", true, [1]int{0}, false},
		{"a", false, [1]int{1}, false},
		{"a", true, [1]int{0}, false},
		{"a\nb,c\n", false, [1]int{2}, false},
		{"a\nb,c\n", true, [1]int{1}, false},
		{"\"", false, [1]int{0}, true},
	}

	for _, test := range tests {
		options := map[string]interface{}{"ContainsHeader": test.containsHeader}
		r, err := (&csv.CSV{}).Init(options, readerstub.NewOutput1D(test.str))
		require.Nil(t, err)

		shape, err := r.(*csv.CSV).Shape()
		assert.Equal(t, test.expectedShape, shape)
		testutils.CheckError(t, test.expectError, err)
	}
}
