package csv

import (
	"encoding/csv"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/ioreader"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"io"
	"strconv"
)

var (
	_ reader.ReadFrom1D = &CSV{}
	_ reader.Output1D   = &CSV{}
)

type CSV struct {
	ReadFrom       reader.Output1D
	ContainsHeader bool
}

func (this *CSV) CanRead(readFrom reader.Output1D) bool {
	_, err := (&CSV{readFrom, false}).Shape()
	return err == nil
}

func (this *CSV) Init(options map[string]interface{}, readFrom reader.Output1D) (reader.ReadFrom1D, error) {
	o := &struct{ ContainsHeader bool }{}
	err := mapstructure.Decode(options, o)
	if err != nil {
		return nil, errors.Wrap(err, "Decode")
	}
	return &CSV{readFrom, o.ContainsHeader}, nil
}

func (this *CSV) Close() {}

func (this *CSV) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	names, err := Names(ioreader.New(this.ReadFrom), this.ContainsHeader)
	if err != nil {
		return nil, errors.Wrap(err, "Names")
	}

	reader := csv.NewReader(ioreader.New(this.ReadFrom))
	reader.FieldsPerRecord = -1
	SkipRows(reader, range1D.From, this.ContainsHeader)

	var records []map[string]interface{}
	for i := uint(0); i < range1D.Size; i++ {
		record, err := NextRecord(reader, names)
		if err != nil {
			if errors.Cause(err) == io.EOF {
				return readresult.Object1D(records), nil
			}

			return nil, errors.Wrap(err, "NextRecord")
		}
		records = append(records, record)
	}
	return readresult.Object1D(records), nil
}

func Names(file io.Reader, containsHeader bool) ([]string, error) {
	reader := csv.NewReader(file)
	reader.FieldsPerRecord = -1

	if containsHeader {
		record, err := reader.Read()
		if err == io.EOF {
			return nil, nil
		} else if err != nil {
			return nil, errors.Wrap(err, "Read")
		}
		return record, nil
	}

	return nil, nil
}

func SkipRows(csvReader *csv.Reader, n uint, containsHeader bool) {
	skip := n
	if containsHeader {
		skip++
	}
	for i := uint(0); i < skip; i++ {
		_, _ = csvReader.Read()
	}
}

// HeaderAt returns the header name at x where x is more than or equal to 0.
func HeaderAt(names []string, x int) string {
	if x < len(names) {
		return names[x]
	}

	return strconv.Itoa(x)
}

func NextRecord(csvReader *csv.Reader, names []string) (map[string]interface{}, error) {
	record := make(map[string]interface{})
	values, err := csvReader.Read()
	if err != nil {
		return nil, errors.Wrap(err, "Read")
	}

	for i, value := range values {
		record[HeaderAt(names, i)] = value
	}
	return record, nil
}

func (this *CSV) Shape() ([1]int, error) {
	reader := csv.NewReader(ioreader.New(this.ReadFrom))
	reader.FieldsPerRecord = -1

	size := 0
	for {
		_, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				if this.ContainsHeader && size > 0 {
					size--
				}
				return [1]int{size}, nil
			}
			return [1]int{}, errors.Wrap(err, "Read")
		}
		size++
	}
}
