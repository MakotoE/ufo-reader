package selector

import (
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFrom1D = &Selector{}
	_ reader.Output1D   = &Selector{}
)

type Selector struct {
	ReadFrom reader.Output1D
	Keys     []string
}

func (this *Selector) CanRead(reader.Output1D) bool {
	return true
}

func (this *Selector) Init(options map[string]interface{}, readFrom reader.Output1D) (reader.ReadFrom1D, error) {
	o := &struct{ Keys []string }{}
	if err := mapstructure.Decode(options, o); err != nil {
		return nil, errors.Wrap(err, "mapstructure.Decode")
	}

	return &Selector{readFrom, o.Keys}, nil
}

func (this *Selector) Close() {
}

func (this *Selector) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	data, err := this.ReadFrom.Read(range1D)
	if err != nil {
		return nil, errors.Wrap(err, "ReadObject1D")
	}
	fromTo := range1D.FromTo(len(data.Object()))
	return readresult.Object1D(Select(data.Object()[fromTo.From:fromTo.To], this.Keys)), nil
}

func Select(from []map[string]interface{}, keys []string) []map[string]interface{} {
	result := make([]map[string]interface{}, len(from))
	for i, obj := range from {
		result[i] = map[string]interface{}{}
		for _, key := range keys {
			if _, ok := obj[key]; ok {
				result[i][key] = obj[key]
			}
		}
	}
	return result
}

func (this *Selector) Shape() ([1]int, error) {
	result, err := this.ReadFrom.Shape()
	return result, errors.Wrap(err, "Shape")
}
