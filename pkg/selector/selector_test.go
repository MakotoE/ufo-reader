package selector_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/pkg/selector"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestSelector_Read(t *testing.T) {
	tests := []struct {
		options     map[string]interface{}
		readFrom    reader.Output1D
		range1D     reader.Range1D
		expected    readresult.Result1D
		expectError bool
	}{
		{
			nil,
			readerstub.NewOutput1D(nil),
			reader.Range1D{0, 0},
			readresult.Object1D{},
			false,
		},
		{
			map[string]interface{}{"Keys": []string{"a"}},
			readerstub.NewOutput1D([]map[string]interface{}{{"a": 0, "b": 1}, {"a": 2, "b": 3}}),
			reader.Range1D{0, 1},
			readresult.Object1D{{"a": 0}},
			false,
		},
		{
			nil,
			readerstub.NewOutput1D(nil),
			reader.Range1D{0, 1},
			readresult.Object1D{},
			false,
		},
	}

	for _, test := range tests {
		s, err := (&selector.Selector{}).Init(test.options, test.readFrom)
		require.Nil(t, err)

		result, err := s.(*selector.Selector).Read(test.range1D)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestSelect(t *testing.T) {
	tests := []struct {
		from     []map[string]interface{}
		keys     []string
		expected []map[string]interface{}
	}{
		{nil, nil, []map[string]interface{}{}},
		{
			[]map[string]interface{}{{"a": 0}},
			nil,
			[]map[string]interface{}{{}},
		},
		{
			[]map[string]interface{}{{"a": 0}},
			[]string{"a"},
			[]map[string]interface{}{{"a": 0}},
		},
		{
			[]map[string]interface{}{{}},
			[]string{"a"},
			[]map[string]interface{}{{}},
		},

		{
			[]map[string]interface{}{{"a": 0}, {"b": 1}},
			[]string{"a"},
			[]map[string]interface{}{{"a": 0}, {}},
		},
	}

	for _, test := range tests {
		result := selector.Select(test.from, test.keys)
		assert.Equal(t, test.expected, result)
	}
}
