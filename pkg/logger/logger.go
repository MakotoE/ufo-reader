package logger

import (
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"sync"
)

type Logger struct {
	Log  []message.IMessage
	Lock sync.Mutex
}

func (this *Logger) Push(msg message.IMessage) {
	this.Lock.Lock()
	defer this.Lock.Unlock()
	this.Log = append(this.Log, msg)
}

func (this *Logger) GetLog() ([]interface{}, error) {
	this.Lock.Lock()
	defer this.Lock.Unlock()
	b, err := json.Marshal(this.Log)
	if err != nil {
		return nil, errors.Wrap(err, "json.Marshal")
	}

	var result []interface{}
	err = json.Unmarshal(b, &result)
	return result, errors.Wrap(err, "json.Unmarshal")
}
