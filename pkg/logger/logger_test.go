package logger_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/logger"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestLogger_Push(t *testing.T) {
	log := &logger.Logger{}
	log.Push(&message.Output{})
	assert.Equal(t, []message.IMessage{&message.Output{}}, log.Log)
}

func TestLogger_GetLog(t *testing.T) {
	tests := []struct {
		log         *logger.Logger
		check       func([]interface{}) bool
		expectError bool
	}{
		{
			&logger.Logger{},
			func(result []interface{}) bool {
				return result == nil
			},
			false,
		},
		{
			&logger.Logger{Log: []message.IMessage{&message.Output{}}},
			func(result []interface{}) bool {
				return result[0].(map[string]interface{})["ChainID"] == 0.0
			},
			false,
		},
	}

	for _, test := range tests {
		result, err := test.log.GetLog()
		assert.True(t, test.check(result))
		testutils.CheckError(t, test.expectError, err)
	}
}
