package sessiondesktop_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/sessiondesktop"
	"testing"
)

func TestHandleMessage(t *testing.T) {
	output := make(chan *message.Output, 1)
	s := sessiondesktop.New(output)
	s.Input(&message.Read{message.Message{"Read"}, 0, 0, nil})
	select {
	case msg := <-output:
		_, ok := msg.Output.(struct{ Error string })
		assert.True(t, ok)
	default:
		assert.Fail(t, "did not receive output")
	}

	s.Input(&message.Version{message.Message{"Version"}})
	select {
	case msg := <-output:
		expected := &message.Output{
			message.Message{"Output"},
			-1,
			struct{ Version string }{Version: "undefined"},
		}
		assert.Equal(t, expected, msg)
	default:
		assert.Fail(t, "did not receive output")
	}
}
