package sessiondesktop

import (
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/logger"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/session"
)

type SessionDesktop struct {
	*session.Session
}

func New(output chan<- *message.Output) *SessionDesktop {
	return &SessionDesktop{&session.Session{session.ChainArray{}, output, &logger.Logger{}}}
}

func (this *SessionDesktop) Input(msg message.IMessage) {
	this.Logger.Push(msg)
	if err := this.RunFunction(msg); err != nil {
		this.OutputChan <- message.NewError(errors.Wrap(err, "RunFunction"))
	}
}

func (this *SessionDesktop) RunFunction(msg message.IMessage) error {
	switch msg.GetID() {
	case "Chain":
		this.Chain(msg.(*message.Chain))
		return nil
	default:
		return this.RunBaseFunction(msg)
	}
}

func (this *SessionDesktop) Chain(msg *message.Chain) {
	this.Chains.GetOrAdd(msg.ChainID, this.OutputChan).Update(msg.ReaderList)
}
