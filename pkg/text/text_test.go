package text_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/pkg/text"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestText_Read(t *testing.T) {
	tests := []struct {
		str            string
		range1D        reader.Range1D
		expectedResult readresult.Result1D
		expectError    bool
	}{
		{
			"",
			reader.Range1D{0, 0},
			readresult.Byte1D{},
			false,
		},
		{
			"",
			reader.Range1D{0, 1},
			readresult.Byte1D{},
			false,
		},
		{
			"",
			reader.Range1D{1, 2},
			readresult.Byte1D{},
			false,
		},
		{
			"a",
			reader.Range1D{0, 1},
			readresult.Byte1D{'a'},
			false,
		},
		{
			" a",
			reader.Range1D{1, 1},
			readresult.Byte1D{'a'},
			false,
		},
	}

	for _, test := range tests {
		r, err := (&text.Text{}).Init(map[string]interface{}{"Str": test.str})
		require.Nil(t, err)
		result, err := r.(*text.Text).Read(test.range1D)
		assert.Equal(t, test.expectedResult, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestText_Shape(t *testing.T) {
	r, _ := (&text.Text{}).Init(map[string]interface{}{"Str": "a"})
	result, err := r.(*text.Text).Shape()
	assert.Equal(t, [1]int{1}, result)
	assert.Nil(t, err)
}
