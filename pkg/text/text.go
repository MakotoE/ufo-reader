package text

import (
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
)

var (
	_ reader.ReadFromNone = &Text{}
	_ reader.Output1D     = &Text{}
)

type Text struct {
	Str string
}

func (this *Text) Init(options map[string]interface{}) (reader.Reader, error) {
	o := &struct{ Str string }{}
	if err := mapstructure.Decode(options, o); err != nil {
		return nil, errors.Wrap(err, "Decode")
	}
	return &Text{o.Str}, nil
}

func (this *Text) Close() {}

func (this *Text) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	fromTo := range1D.FromTo(len(this.Str))
	return readresult.Byte1D(this.Str[fromTo.From:fromTo.To]), nil
}

func (this *Text) Shape() ([1]int, error) {
	return [1]int{len(this.Str)}, nil
}
