package message

import "reflect"

type Output struct {
	Message
	ChainID int
	Output  interface{}
}

func NewOutput(chainID int, output interface{}) *Output {
	return &Output{Message{"Output"}, chainID, output}
}

func NewError(err error) *Output {
	return &Output{Message{"Output"}, -1, struct{ Error string }{err.Error()}}
}

type Version struct {
	Message
}

// File signals that subsequent binary messages are file data, with Name and for ChainID.
type File struct {
	Message
	ChainID int
	Name    string
}

type Chain struct {
	Message
	ChainID    int
	ReaderList []ReaderListItem
}

type Read struct {
	Message
	ChainID int
	Index   int
	Range   map[string]interface{}
}

type Shape struct {
	Message
	ChainID int
	Index   int
}

type Subscribe Read

type Unsubscribe struct {
	Message
	ChainID int
}

type Log struct {
	Message
}

type ReaderListItem struct {
	Name    string
	Options map[string]interface{}
}

func (this *ReaderListItem) Equals(other ReaderListItem) bool {
	return this.Name == other.Name && reflect.DeepEqual(this.Options, other.Options)
}
