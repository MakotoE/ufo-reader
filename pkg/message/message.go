package message

import (
	"encoding/json"
	"github.com/pkg/errors"
)

type IMessage interface {
	GetID() string
}

type Message struct {
	ID string
}

func (this *Message) GetID() string {
	return this.ID
}

func New(id string) (IMessage, error) {
	switch id {
	case "Message":
		return &Message{}, nil
	case "Output":
		return &Output{}, nil
	case "Version":
		return &Version{}, nil
	case "File":
		return &File{}, nil
	case "Chain":
		return &Chain{}, nil
	case "Read":
		return &Read{}, nil
	case "Shape":
		return &Shape{}, nil
	case "Subscribe":
		return &Subscribe{}, nil
	case "Unsubscribe":
		return &Unsubscribe{}, nil
	case "Log":
		return &Log{}, nil
	default:
		return nil, errors.New("message type with id does not exist: " + id)
	}
}

func Deserialize(b []byte) (IMessage, error) {
	msg := &Message{}
	if err := json.Unmarshal(b, msg); err != nil {
		return nil, errors.Wrap(err, "first Unmarshal")
	}

	result, err := New(msg.GetID())
	if err != nil {
		return nil, errors.Wrap(err, "message.New")
	}

	if err := json.Unmarshal(b, result); err != nil {
		return nil, errors.Wrap(err, "second Unmarshal")
	}
	return result, nil
}
