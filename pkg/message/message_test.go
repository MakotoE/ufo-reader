package message_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestNewOutput(t *testing.T) {
	assert.Equal(t, &message.Output{message.Message{"Output"}, 0, 1}, message.NewOutput(0, 1))
}

func TestDeserialize(t *testing.T) {
	tests := []struct {
		json        string
		expected    interface{}
		expectError bool
	}{
		{`{"ID": "Message"}`, &message.Message{ID: "Message"}, false},
		{`{"ID": "Output", "ChainID": "a"}`, nil, true},
		{``, nil, true},
	}

	for _, test := range tests {
		result, err := message.Deserialize([]byte(test.json))
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}
