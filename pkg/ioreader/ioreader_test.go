package ioreader_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/ioreader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readerstub"
	"io"
	"testing"
)

func TestIOReader_Read(t *testing.T) {
	tests := []struct {
		ioreader.IOReader
		p             []byte
		expected      int
		expectedError error
		expectedP     []byte
		expectedPos   int
	}{
		{ // length 0, pos 0, read 0
			ioreader.IOReader{readerstub.NewOutput1D(nil), 0},
			nil,
			0,
			nil,
			nil,
			0,
		},
		{ // length 0, pos 0, read 1
			ioreader.IOReader{readerstub.NewOutput1D(nil), 0},
			make([]byte, 1),
			0,
			io.EOF,
			[]byte{0},
			0,
		},
		{ // length 1, pos 0, read 0
			ioreader.IOReader{readerstub.NewOutput1D([]byte{1}), 0},
			make([]byte, 0),
			0,
			nil,
			[]byte{},
			0,
		},
		{ // length 1, pos 0, read 1
			ioreader.IOReader{readerstub.NewOutput1D([]byte{1}), 0},
			make([]byte, 1),
			1,
			nil,
			[]byte{1},
			1,
		},
		{ // length 1, pos 1, read 0
			ioreader.IOReader{readerstub.NewOutput1D([]byte{1}), 1},
			make([]byte, 0),
			0,
			nil,
			[]byte{},
			1,
		},
		{ // length 1, pos 1, read 1
			ioreader.IOReader{readerstub.NewOutput1D([]byte{1}), 1},
			make([]byte, 1),
			0,
			io.EOF,
			[]byte{0},
			1,
		},
		{ // length 1, pos 0, read 2
			ioreader.IOReader{readerstub.NewOutput1D([]byte{1}), 0},
			make([]byte, 2),
			1,
			nil,
			[]byte{1, 0},
			1,
		},
		{ // length 2, pos 0, read 1
			ioreader.IOReader{readerstub.NewOutput1D([]byte{1, 2}), 0},
			make([]byte, 1),
			1,
			nil,
			[]byte{1},
			1,
		},
		{ // length 2, pos 0, read 2
			ioreader.IOReader{readerstub.NewOutput1D([]byte{1, 2}), 0},
			make([]byte, 2),
			2,
			nil,
			[]byte{1, 2},
			2,
		},
		{ // length 2, pos 1, read 1
			ioreader.IOReader{readerstub.NewOutput1D([]byte{1, 2}), 1},
			make([]byte, 1),
			1,
			nil,
			[]byte{2},
			2,
		},
		{ // length 1, pos 2, read 1 (this shouldn't happen)
			ioreader.IOReader{readerstub.NewOutput1D([]byte{1}), 2},
			make([]byte, 1),
			0,
			io.EOF,
			[]byte{0},
			2,
		},
	}

	for _, test := range tests {
		result, err := test.IOReader.Read(test.p)
		assert.Equal(t, test.expected, result)
		assert.Equal(t, test.expectedError, err)
		assert.Equal(t, test.expectedP, test.p)
		assert.Equal(t, test.expectedPos, test.IOReader.Pos)
	}
}
