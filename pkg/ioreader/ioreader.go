package ioreader

import (
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"io"
)

func New(reader1D reader.Output1D) io.Reader {
	if ioReader, ok := reader1D.(reader.IOReader); ok {
		return ioReader.IOReader()
	}
	return &IOReader{reader1D, 0}
}

type IOReader struct {
	ReadFrom reader.Output1D
	Pos      int
}

func (this *IOReader) Read(p []byte) (int, error) {
	data, err := this.ReadFrom.Read(reader.Range1D{uint(this.Pos), uint(len(p))})
	if err != nil {
		return 0, errors.Wrap(err, "Read")
	}
	if len(p) != 0 && len(data.Byte()) == 0 {
		return 0, io.EOF
	}

	copy(p, data.Byte())
	this.Pos += len(data.Byte())
	return len(data.Byte()), nil
}
