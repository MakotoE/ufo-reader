package png

import (
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/ioreader"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"image"
	"image/png"
)

var (
	_ reader.ReadFrom1D = &PNG{}
	_ reader.Output2D   = &PNG{}
)

type PNG struct {
	Image image.Image
}

func (this *PNG) CanRead(readFrom reader.Output1D) bool {
	_, err := png.Decode(ioreader.New(readFrom))
	return err == nil
}

func (this *PNG) Init(_ map[string]interface{}, readFrom reader.Output1D) (reader.ReadFrom1D, error) {
	img, err := png.Decode(ioreader.New(readFrom))
	if err != nil {
		return nil, errors.Wrap(err, "Decode")
	}
	return &PNG{img}, nil
}

func (this *PNG) Close() {}

func (this *PNG) Read(range2D reader.Range2D) (readresult.Result2D, error) {
	shape, err := this.Shape()
	if err != nil {
		return nil, errors.Wrap(err, "Shape")
	}

	area, err := reader.NewArea(shape[0], shape[1])
	if err != nil {
		return nil, errors.Wrap(err, "NewArea")
	}

	readRange := range2D.Intersection(reader.Range2D{reader.Coordinate2D{0, 0}, area})

	data := make(readresult.Object2D, readRange.Area.Rows)
	for row := 0; row < len(data); row++ {
		data[row] = make([]map[string]interface{}, readRange.Area.Cols)
		for column := 0; column < len(data[row]); column++ {
			x, y := column+readRange.TopLeft.Col, row+readRange.TopLeft.Row
			// This should return the correct color palette instead of converting to rgba
			r, g, b, a := this.Image.At(x, y).RGBA()
			data[row][column] = map[string]interface{}{"R": r, "G": g, "B": b, "A": a}
		}
	}

	return data, nil
}

func (this *PNG) Shape() ([2]int, error) {
	return [2]int{this.Image.Bounds().Max.Y, this.Image.Bounds().Max.X}, nil
}
