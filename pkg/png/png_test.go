package png_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/file"
	"gitlab.com/MakotoE/ufo-reader/pkg/png"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"testing"
)

func TestPNG_CanRead(t *testing.T) {
	tests := []struct {
		filename string
		expected bool
	}{
		{"1x1.png", true},
		{"empty", false},
	}

	for _, test := range tests {
		options := map[string]interface{}{"Path": testutils.Dir() + "/" + test.filename}
		f, err := (&file.File{}).Init(options)
		require.Nil(t, err)
		result := (&png.PNG{}).CanRead(f.(*file.File))
		assert.Equal(t, test.expected, result)
	}
}

func TestPNG_Read(t *testing.T) {
	tests := []struct {
		filename       string
		range2D        reader.Range2D
		expectedResult readresult.Result2D
		expectError    bool
	}{
		{
			"1x1.png",
			reader.Range2D{
				reader.Coordinate2D{0, 0},
				reader.Area{0, 0},
			},
			readresult.Object2D{},
			false,
		},
		{
			"1x1.png",
			reader.Range2D{
				reader.Coordinate2D{0, 0},
				reader.Area{0, 1},
			},
			readresult.Object2D{},
			false,
		},
		{
			"1x1.png",
			reader.Range2D{
				reader.Coordinate2D{0, 0},
				reader.Area{1, 0},
			},
			readresult.Object2D{{}},
			false,
		},
		{
			"1x1.png",
			reader.Range2D{
				reader.Coordinate2D{0, 0},
				reader.Area{1, 1},
			},
			readresult.Object2D{{{
				"R": uint32(0xffff),
				"G": uint32(0xffff),
				"B": uint32(0xffff),
				"A": uint32(0xffff),
			}}},
			false,
		},
		{
			"1x1.png",
			reader.Range2D{
				reader.Coordinate2D{-1, -1},
				reader.Area{2, 2},
			},
			readresult.Object2D{{
				{
					"R": uint32(0xffff),
					"G": uint32(0xffff),
					"B": uint32(0xffff),
					"A": uint32(0xffff),
				},
			}},
			false,
		},
		{
			"1x1.png",
			reader.Range2D{
				reader.Coordinate2D{1, 1},
				reader.Area{1, 1},
			},
			readresult.Object2D{},
			false,
		},
		{
			"2x2.png",
			reader.Range2D{
				reader.Coordinate2D{0, 0},
				reader.Area{2, 2},
			},
			readresult.Object2D{
				{
					{
						"R": uint32(0xffff),
						"G": uint32(0),
						"B": uint32(0),
						"A": uint32(0xffff),
					},
					{
						"R": uint32(0),
						"G": uint32(0),
						"B": uint32(0),
						"A": uint32(0),
					},
				},
				{
					{
						"R": uint32(0),
						"G": uint32(0),
						"B": uint32(0xffff),
						"A": uint32(0xffff),
					},
					{
						"R": uint32(0),
						"G": uint32(0),
						"B": uint32(0),
						"A": uint32(0),
					},
				},
			},
			false,
		},
		{
			"2x2.png",
			reader.Range2D{
				reader.Coordinate2D{1, 0},
				reader.Area{1, 1},
			},
			readresult.Object2D{{
				{"R": uint32(0), "G": uint32(0), "B": uint32(0xffff), "A": uint32(0xffff)},
			}},
			false,
		},
	}

	for _, test := range tests {
		options := map[string]interface{}{"Path": testutils.Dir() + "/" + test.filename}
		f, err := (&file.File{}).Init(options)
		require.Nil(t, err)

		r, err := (&png.PNG{}).Init(nil, f.(*file.File))
		assert.Nil(t, err)

		result, err := r.(*png.PNG).Read(test.range2D)
		assert.Equal(t, test.expectedResult, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestPNG_Shape2D(t *testing.T) {
	options := map[string]interface{}{"Path": testutils.Dir() + "/1x1.png"}
	f, err := (&file.File{}).Init(options)
	require.Nil(t, err)
	r, err := (&png.PNG{}).Init(nil, f.(*file.File))
	require.Nil(t, err)

	result, err := r.(*png.PNG).Shape()
	assert.Equal(t, [2]int{1, 1}, result)
	assert.Nil(t, err)
}
