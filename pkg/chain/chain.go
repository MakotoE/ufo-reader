package chain

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/readabilitydetector"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readermap"
	"reflect"
	"strconv"
	"sync"
)

func init() {
	// Adds ReadablityDetector to readermap.Map without running into import cycle issues
	readermap.Map["ReadablityDetector"] = &readabilitydetector.ReadablityDetector{}
}

type Chain struct {
	ChainID         int
	Readers         ReaderArray
	ReaderList      []message.ReaderListItem
	Output          chan<- *message.Output
	UnsubscribeChan chan bool
	Lock            sync.Mutex
}

func New(chainID int, output chan<- *message.Output) *Chain {
	return &Chain{
		chainID,
		nil,
		nil,
		output,
		make(chan bool, 1),
		sync.Mutex{},
	}
}

func (this *Chain) Close() {
	this.Unsubscribe()
	CloseReaders(this.Readers, &this.Lock)
}

func CloseReaders(readers []reader.Reader, lock *sync.Mutex) {
	lock.Lock()
	defer lock.Unlock()
	for _, reader := range readers {
		reader.Close()
	}
}

func (this *Chain) Update(readerList []message.ReaderListItem) {
	i := FirstDifference(this.ReaderList, readerList)
	this.Unsubscribe()
	CloseReaders(this.Readers[i:], &this.Lock)

	readers, err := NewReaders(this.Readers[:i], readerList[i:])
	if err != nil {
		this.Output <- message.NewError(errors.Wrap(err, "New"))
	} else {
		this.Readers = readers
		this.ReaderList = readerList
	}
}

func NewReaders(chain []reader.Reader, readersToAdd []message.ReaderListItem) ([]reader.Reader, error) {
	for _, readerListItem := range readersToAdd {
		reader, err := NewReader(LastReader(chain), readerListItem)
		if err != nil {
			return nil, errors.Wrap(err, "NewReader")
		}
		chain = append(chain, reader)
	}
	return chain, nil
}

func NewReader(lastReader reader.Reader, readerListItem message.ReaderListItem) (reader.Reader, error) {
	readerStruct, ok := readermap.Map[readerListItem.Name]
	if !ok {
		return nil, errors.New("Reader does not exist: " + readerListItem.Name)
	}

	if newReaderType, ok := readerStruct.(reader.ReadFromNone); ok {
		reader, err := newReaderType.Init(readerListItem.Options)
		return reader, errors.Wrap(err, "Init")
	}

	// If lastReader is nil, the new Reader is Reader 0. Reader 0 must be
	// ReadFromNone.
	if lastReader == nil {
		return nil, errors.New(
			"readerStruct is not ReadFromNone: " + fmt.Sprintf("%#v", readerStruct),
		)
	}

	if readFromAny, ok := readerStruct.(reader.ReadFromAny); ok {
		reader, err := readFromAny.Init(readerListItem.Options, lastReader)
		return reader, errors.Wrap(err, "Init")
	}
	if readFrom0, ok := readerStruct.(reader.ReadFrom0D); ok {
		if readFrom, ok := lastReader.(reader.Output0D); ok {
			reader, err := readFrom0.Init(readerListItem.Options, readFrom)
			return reader, errors.Wrap(err, "Init")
		}
	} else if readFrom1D, ok := readerStruct.(reader.ReadFrom1D); ok {
		if readFrom, ok := lastReader.(reader.Output1D); ok {
			reader, err := readFrom1D.Init(readerListItem.Options, readFrom)
			return reader, errors.Wrap(err, "Init")
		}
	} else if readFrom2D, ok := readerStruct.(reader.ReadFrom2D); ok {
		if readFrom, ok := lastReader.(reader.Output2D); ok {
			reader, err := readFrom2D.Init(readerListItem.Options, readFrom)
			return reader, errors.Wrap(err, "Init")
		}
	}

	return nil, errors.New(
		"incompatible types: " + fmt.Sprintf("%T", lastReader) + ", " +
			fmt.Sprintf("%T", readerStruct),
	)
}

func FirstDifference(a, b []message.ReaderListItem) int {
	i := 0
	for ; i < len(b); i++ {
		if i == len(a) || !a[i].Equals(b[i]) {
			return i
		}
	}
	return i
}

func LastReader(chain []reader.Reader) reader.Reader {
	if len(chain) == 0 {
		return nil
	}
	return chain[len(chain)-1]
}

func (this *Chain) Read(index int, r map[string]interface{}) {
	this.Lock.Lock()
	defer this.Lock.Unlock()

	readerAtIndex, err := this.Readers.Get(index)
	if err != nil {
		this.Output <- message.NewError(errors.Wrap(err, "Get"))
		return
	}

	output, err := Read(readerAtIndex, r)
	if err != nil {
		this.Output <- message.NewError(errors.Wrap(err, "Read"))
	} else {
		this.Output <- message.NewOutput(this.ChainID, output)
	}
}

func Read(readerAtIndex reader.Reader, r map[string]interface{}) (interface{}, error) {
	if reader0D, ok := readerAtIndex.(reader.Output0D); ok {
		result, err := reader0D.Read()
		return result, errors.Wrap(err, "Read")
	}

	if reader1D, ok := readerAtIndex.(reader.Output1D); ok {
		range1D := &reader.Range1D{}
		if err := mapstructure.Decode(r, range1D); err != nil {
			return nil, errors.Wrap(err, "Decode")
		}

		result, err := reader1D.Read(*range1D)
		return result, errors.Wrap(err, "Read")
	}

	if reader2D, ok := readerAtIndex.(reader.Output2D); ok {
		range2D := &reader.Range2D{}
		if err := mapstructure.Decode(r, range2D); err != nil {
			return nil, errors.Wrap(err, "Decode")
		}

		result, err := reader2D.Read(*range2D)
		return result, errors.Wrap(err, "Read")
	}

	return nil, errors.New(
		"Reader type for readerAtIndex is not implemented: " + fmt.Sprintf("%T", readerAtIndex),
	)
}

func (this *Chain) Shape(index int) {
	this.Lock.Lock()
	defer this.Lock.Unlock()

	readerAtIndex, err := this.Readers.Get(index)
	if err != nil {
		this.Output <- message.NewError(errors.Wrap(err, "Get"))
		return
	}

	output, err := Shape(readerAtIndex)
	if err != nil {
		this.Output <- message.NewError(errors.Wrap(err, "Read"))
	} else {
		this.Output <- message.NewOutput(this.ChainID, output)
	}
}

func Shape(readerAtIndex reader.Reader) (interface{}, error) {
	if _, ok := readerAtIndex.(reader.Output0D); ok {
		return [0]int{}, nil
	}
	if reader1D, ok := readerAtIndex.(reader.Output1D); ok {
		result, err := reader1D.Shape()
		return result, errors.Wrap(err, "Shape")
	}
	if reader2D, ok := readerAtIndex.(reader.Output2D); ok {
		result, err := reader2D.Shape()
		return result, errors.Wrap(err, "Shape")
	}

	return nil, errors.New("Reader type is not implemented: " + fmt.Sprintf("%T", readerAtIndex))
}

func (this *Chain) Subscribe(index int, r map[string]interface{}) {
	readerAtIndex, err := this.Readers.Get(index)
	if err != nil {
		this.Output <- message.NewError(errors.Wrap(err, "Get"))
		return
	}

	subscriber, ok := readerAtIndex.(reader.Subscriber)
	if !ok {
		s := "reader is not a subscriber: " + reflect.TypeOf(readerAtIndex).Name()
		this.Output <- message.NewError(errors.New(s))
		return
	}

	update := make(chan bool, 5)
	this.UnsubscribeChan = make(chan bool, 1) // Empties channel
	subscriber.Subscribe(update, this.UnsubscribeChan)
	go this.ReadCaller(update, index, r)
}

func (this *Chain) Unsubscribe() {
	select {
	case this.UnsubscribeChan <- true:
	default:
	}
}

func (this *Chain) ReadCaller(update <-chan bool, index int, r map[string]interface{}) {
	for {
		select {
		case <-update:
			this.Read(index, r)
		case <-this.UnsubscribeChan:
			return
		}
	}
}

type ReaderArray []reader.Reader

func (this ReaderArray) Get(index int) (reader.Reader, error) {
	if !(0 <= index && index < len(this)) {
		return nil, errors.New("index is out of range: " + strconv.Itoa(index))
	}
	return this[index], nil
}
