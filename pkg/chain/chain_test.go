package chain_test

import (
	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/averageobject"
	"gitlab.com/MakotoE/ufo-reader/pkg/chain"
	"gitlab.com/MakotoE/ufo-reader/pkg/csv"
	"gitlab.com/MakotoE/ufo-reader/pkg/currentdate"
	"gitlab.com/MakotoE/ufo-reader/pkg/file"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/png"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/pkg/text"
	"gitlab.com/MakotoE/ufo-reader/test/mocks/mock_reader"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"sync"
	"testing"
	"time"
)

func TestReader_Equals(t *testing.T) {
	tests := []struct {
		readerA        message.ReaderListItem
		readerB        message.ReaderListItem
		expectedResult bool
	}{
		{
			message.ReaderListItem{"a", map[string]interface{}{}},
			message.ReaderListItem{"a", map[string]interface{}{}},
			true,
		},
		{
			message.ReaderListItem{"a", map[string]interface{}{}},
			message.ReaderListItem{"b", map[string]interface{}{}},
			false,
		},
		{
			message.ReaderListItem{"a", map[string]interface{}{}},
			message.ReaderListItem{"a", map[string]interface{}{"a": 0}},
			false,
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expectedResult, test.readerA.Equals(test.readerB))
	}
}

func TestCloseReaders(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockReader := mock_reader.NewMockReader(ctrl)
	mockReader.EXPECT().Close().Times(1)

	chain.CloseReaders(chain.ReaderArray{mockReader}, &sync.Mutex{})
}

func TestChain_Update(t *testing.T) {
	output := make(chan *message.Output, 1)

	tests := []struct {
		chain           chain.Chain
		inputReaderList []message.ReaderListItem
		expectedChain   chain.Chain
		expectError     bool
	}{
		{
			chain.Chain{Output: output},
			nil,
			chain.Chain{Output: output},
			false,
		},
		{
			chain.Chain{Output: output},
			[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": ""}}},
			chain.Chain{
				0,
				[]reader.Reader{&text.Text{""}},
				[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": ""}}},
				output,
				nil,
				sync.Mutex{},
			},
			false,
		},
		{
			chain.Chain{
				0,
				[]reader.Reader{&text.Text{"existing reader"}},
				[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": ""}}},
				output,
				nil,
				sync.Mutex{},
			},
			[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": ""}}},
			chain.Chain{
				0,
				[]reader.Reader{&text.Text{"existing reader"}},
				[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": ""}}},
				output,
				nil,
				sync.Mutex{},
			},
			false,
		},
		{
			chain.Chain{
				0,
				[]reader.Reader{&text.Text{""}},
				[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": ""}}},
				output,
				nil,
				sync.Mutex{},
			},
			[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": "new Reader"}}},
			chain.Chain{
				0,
				[]reader.Reader{&text.Text{"new Reader"}},
				[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": "new Reader"}}},
				output,
				nil,
				sync.Mutex{},
			},
			false,
		},
		{
			chain.Chain{
				0,
				[]reader.Reader{&text.Text{"existing reader"}},
				[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": ""}}},
				output,
				nil,
				sync.Mutex{},
			},
			[]message.ReaderListItem{
				{"Text", map[string]interface{}{"Str": ""}},
				{"Text", map[string]interface{}{"Str": "new reader"}},
			},
			chain.Chain{
				0,
				[]reader.Reader{&text.Text{"existing reader"}, &text.Text{"new reader"}},
				[]message.ReaderListItem{
					{"Text", map[string]interface{}{"Str": ""}},
					{"Text", map[string]interface{}{"Str": "new reader"}},
				},
				output,
				nil,
				sync.Mutex{},
			},
			false,
		},
		{
			chain.Chain{Output: output},
			[]message.ReaderListItem{{}},
			chain.Chain{Output: output, Lock: sync.Mutex{}},
			true,
		},
	}

	for _, test := range tests {
		test.chain.Update(test.inputReaderList)
		assert.Equal(t, test.expectedChain, test.chain)
		select {
		case <-output:
			assert.True(t, test.expectError)
		default:
			assert.False(t, test.expectError)
		}
	}
}

func TestNewReaders(t *testing.T) {
	tests := []struct {
		chain         []reader.Reader
		readersToAdd  []message.ReaderListItem
		expectedChain []reader.Reader
		expectError   bool
	}{
		{
			nil,
			nil,
			nil,
			false,
		},
		{
			nil,
			[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": "a"}}},
			[]reader.Reader{&text.Text{"a"}},
			false,
		},
		{
			[]reader.Reader{&text.Text{"a"}},
			[]message.ReaderListItem{{"Text", map[string]interface{}{"Str": "b"}}},
			[]reader.Reader{&text.Text{"a"}, &text.Text{"b"}},
			false,
		},
		{
			nil,
			[]message.ReaderListItem{{}},
			nil,
			true,
		},
		{
			[]reader.Reader{&averageobject.AverageObject{}},
			[]message.ReaderListItem{{"CSV", nil}},
			nil,
			true,
		},
	}

	for _, test := range tests {
		result, err := chain.NewReaders(test.chain, test.readersToAdd)
		assert.Equal(t, test.expectedChain, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestNewReader(t *testing.T) {
	tests := []struct {
		lastReader     reader.Reader
		readerListItem message.ReaderListItem
		expected       reader.Reader
		expectError    bool
	}{
		{
			&csv.CSV{},
			message.ReaderListItem{"", nil},
			nil,
			true,
		},
		{
			&csv.CSV{},
			message.ReaderListItem{"CSV", nil},
			&csv.CSV{&csv.CSV{}, false},
			false,
		},
		{
			&csv.CSV{},
			message.ReaderListItem{"CSV", map[string]interface{}{"ContainsHeader": true}},
			&csv.CSV{&csv.CSV{}, true},
			false,
		},
		{ // ReadFromNone as next Reader
			&text.Text{},
			message.ReaderListItem{"CurrentDate", nil},
			&currentdate.CurrentDate{},
			false,
		},
		{ // ReadFromNone as lastReader
			nil,
			message.ReaderListItem{"CSV", nil},
			nil,
			true,
		},
	}

	for _, test := range tests {
		result, err := chain.NewReader(test.lastReader, test.readerListItem)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestFirstDifference(t *testing.T) {
	tests := []struct {
		a        []message.ReaderListItem
		b        []message.ReaderListItem
		expected int
	}{
		{
			nil,
			nil,
			0,
		},
		{
			[]message.ReaderListItem{{}},
			nil,
			0,
		},
		{
			nil,
			[]message.ReaderListItem{{}},
			0,
		},
		{
			[]message.ReaderListItem{{}},
			[]message.ReaderListItem{{}},
			1,
		},
		{
			[]message.ReaderListItem{{}, {}},
			[]message.ReaderListItem{{}},
			1,
		},
		{
			[]message.ReaderListItem{{}},
			[]message.ReaderListItem{{}, {}},
			1,
		},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, chain.FirstDifference(test.a, test.b))
	}
}

func TestLastReader(t *testing.T) {
	assert.Equal(t, nil, chain.LastReader(nil))
	assert.Equal(t, &text.Text{}, chain.LastReader([]reader.Reader{&text.Text{}}))
}

func TestChain_Read(t *testing.T) {
	output := make(chan *message.Output, 1)

	tests := []struct {
		chain    *chain.Chain
		index    int
		r        map[string]interface{}
		expected *message.Output
	}{
		{
			&chain.Chain{
				Output:  output,
				Readers: []reader.Reader{&text.Text{"A"}, &text.Text{"B"}},
				Lock:    sync.Mutex{},
			},
			0,
			map[string]interface{}{"From": 0, "Size": 1},
			message.NewOutput(0, readresult.Byte1D{'A'}),
		},
		//{
		//	&chain.Chain{
		//		Output:  output,
		//		Readers: []reader.Reader{},
		//		Lock:    sync.Mutex{},
		//	},
		//	0,
		//	nil,
		//	message.NewError(errors.New("Get: index is out of range: 0")),
		//},
	}

	for _, test := range tests {
		test.chain.Read(test.index, test.r)
		select {
		case result := <-output:
			assert.Equal(t, test.expected, result)
		default:
			assert.Fail(t, "did not receive output")
		}
	}
}

func TestRead(t *testing.T) {
	f, err := (&file.File{}).Init(map[string]interface{}{"Path": testutils.Dir() + "/1x1.png"})
	require.Nil(t, err)
	testImage, err := (&png.PNG{}).Init(nil, f.(*file.File))
	require.Nil(t, err)

	tests := []struct {
		readerAtIndex reader.Reader
		r             map[string]interface{}
		expected      interface{}
		expectError   bool
	}{
		{
			&averageobject.AverageObject{&text.Text{}},
			nil,
			readresult.Object0D{},
			false,
		},
		{
			&text.Text{"Str"},
			nil,
			readresult.Byte1D{},
			false,
		},
		{
			&text.Text{"Str"},
			map[string]interface{}{"From": 0, "Size": 1},
			readresult.Byte1D{'S'},
			false,
		},
		{
			testImage,
			map[string]interface{}{
				"TopLeft": map[string]interface{}{"Col": 0, "Row": 0},
				"Area":    map[string]interface{}{"Cols": 1, "Rows": 1},
			},
			readresult.Object2D{
				{{
					"R": uint32(0xffff),
					"G": uint32(0xffff),
					"B": uint32(0xffff),
					"A": uint32(0xffff),
				}},
			},
			false,
		}, {
			&text.Text{"Str"},
			map[string]interface{}{"From": -1},
			nil,
			true,
		},
		{
			&text.Text{"Str"},
			map[string]interface{}{"From": "a"},
			nil,
			true,
		},
	}

	for _, test := range tests {
		result, err := chain.Read(test.readerAtIndex, test.r)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestChain_Shape(t *testing.T) {
	output := make(chan *message.Output, 1)

	tests := []struct {
		chain    chain.Chain
		index    int
		expected interface{}
	}{
		{
			chain.Chain{
				Readers: []reader.Reader{&text.Text{}},
				Output:  output,
				Lock:    sync.Mutex{},
			},
			0,
			message.NewOutput(0, [1]int{0}),
		},
		{
			chain.Chain{
				Output: output,
				Lock:   sync.Mutex{},
			},
			0,
			message.NewError(errors.New("Get: index is out of range: 0")),
		},
	}

	for _, test := range tests {
		test.chain.Shape(test.index)
		select {
		case result := <-output:
			assert.Equal(t, test.expected, result)
		default:
			assert.Fail(t, "did not receive output")
		}
	}
}

func TestShape(t *testing.T) {
	f, err := (&file.File{}).Init(map[string]interface{}{"Path": testutils.Dir() + "/1x1.png"})
	require.Nil(t, err)
	testImage, err := (&png.PNG{}).Init(nil, f.(*file.File))
	require.Nil(t, err)

	tests := []struct {
		readerAtIndex reader.Reader
		expected      interface{}
	}{
		{
			&averageobject.AverageObject{},
			[0]int{},
		},
		{
			&text.Text{},
			[1]int{0},
		},
		{
			testImage,
			[2]int{1, 1},
		},
	}

	for _, test := range tests {
		result, err := chain.Shape(test.readerAtIndex)
		assert.Equal(t, test.expected, result)
		assert.Nil(t, err)
	}
}

var _ reader.Output1D = &SubscriberStub{}

type SubscriberStub struct{}

func (this *SubscriberStub) Init(options map[string]interface{}) (reader.Reader, error) {
	return &SubscriberStub{}, nil
}

func (this *SubscriberStub) Close() {
}

func (this *SubscriberStub) Subscribe(update chan<- bool, unsubscribe <-chan bool) {
	update <- true
}

func (this *SubscriberStub) Read(reader.Range1D) (readresult.Result1D, error) {
	return readresult.Object1D{map[string]interface{}{"a": "read result"}}, nil
}

func (this *SubscriberStub) Shape() ([1]int, error) {
	return [1]int{}, nil
}

func TestChain_Subscribe(t *testing.T) {
	tests := []struct {
		readers         chain.ReaderArray
		index           int
		r               map[string]interface{}
		expectedMessage *message.Output
		expectError     bool
	}{
		{
			chain.ReaderArray{&SubscriberStub{}},
			0,
			nil,
			message.NewOutput(0, readresult.Object1D{map[string]interface{}{"a": "read result"}}),
			false,
		},
		{
			nil,
			0,
			nil,
			nil,
			true,
		}, {
			chain.ReaderArray{&text.Text{}},
			0,
			nil,
			nil,
			true,
		},
	}

	for _, test := range tests {
		output := make(chan *message.Output, 1)
		c := chain.New(0, output)
		c.Readers = test.readers
		c.Subscribe(test.index, test.r)

		select {
		case msg := <-output:
			c.Unsubscribe()
			if test.expectError {
				_, ok := msg.Output.(struct{ Error string })
				assert.True(t, ok)
			} else {
				assert.Equal(t, test.expectedMessage, msg)
			}
		case <-time.After(time.Second):
			assert.Fail(t, "did not receive output")
		}
	}
}

func TestChain_Unsubscribe(t *testing.T) {
	c := chain.New(0, nil)
	assert.Equal(t, 0, len(c.UnsubscribeChan))
	c.Unsubscribe()
	assert.Equal(t, 1, len(c.UnsubscribeChan))
	c.Unsubscribe()
	assert.Equal(t, 1, len(c.UnsubscribeChan))
}

func TestChain_ReadCaller(t *testing.T) {
	output := make(chan *message.Output, 1)
	c := chain.New(0, output)
	c.Readers = chain.ReaderArray{&text.Text{"a"}}

	update := make(chan bool, 1)
	go c.ReadCaller(update, 0, map[string]interface{}{"Size": 1})
	update <- true

	select {
	case msg := <-output:
		c.Unsubscribe()
		assert.Equal(t, message.NewOutput(0, readresult.Byte1D{97}), msg)
	case <-time.After(time.Second):
		assert.Fail(t, "did not receive output")
	}
}

func TestReaderArray_Get(t *testing.T) {
	tests := []struct {
		readerArray chain.ReaderArray
		index       int
		expected    reader.Reader
		expectError bool
	}{
		{
			chain.ReaderArray{&text.Text{}},
			0,
			&text.Text{},
			false,
		},
		{
			nil,
			0,
			nil,
			true,
		},
	}

	for _, test := range tests {
		result, err := test.readerArray.Get(test.index)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}
