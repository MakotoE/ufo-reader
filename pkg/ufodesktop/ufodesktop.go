package ufodesktop

import (
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/sessiondesktop"
	"gitlab.com/MakotoE/ufo-reader/pkg/version"
	"io"
	"os"
	"os/signal"
	"strconv"
)

func Main() {
	printVersion := flag.Bool("version", false, "prints version")
	flag.Parse()
	if *printVersion {
		fmt.Println(version.Version)
	} else {
		RunDesktop()
	}
}

func RunDesktop() {
	input := make(chan message.IMessage, 5)
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	exit := make(chan bool)

	go ProcessLoop(input, stop, exit)
	go InputLoop(input)

	<-exit
}

func ProcessLoop(input <-chan message.IMessage, close <-chan os.Signal, exit chan<- bool) {
	output := make(chan *message.Output, 5)
	session := sessiondesktop.New(output)
	defer func() {
		session.Close()
		exit <- true
	}()

	for {
		select {
		case msg := <-input:
			session.Input(msg)
		case out := <-output:
			OutputHandler(out, os.Stdout)
		case signal := <-close:
			if signal == os.Interrupt {
				goto breakLoop
			}
		}
	}

breakLoop:
}

func InputLoop(input chan<- message.IMessage) {
	for {
		msg, err := ProcessInput()
		if err != nil {
			OutputHandler(message.NewError(err), os.Stdout)
		} else {
			input <- msg
		}
	}
}

func ProcessInput() (message.IMessage, error) {
	n, err := ReadDataLength(os.Stdin)
	if err != nil {
		return nil, errors.Wrap(err, "ReadDataLength")
	}

	data := make([]byte, n)
	if _, err := os.Stdin.Read(data); err != nil {
		return nil, errors.Wrap(err, "os.Stdin.Read")
	}

	msg, err := message.Deserialize(data)
	if err != nil {
		return nil, errors.Wrap(err, "Deserialize")
	}

	return msg, nil
}

func ReadDataLength(stdin io.Reader) (int, error) {
	prefix := make([]byte, 8)
	n, err := stdin.Read(prefix)
	if err != nil {
		return 0, errors.Wrap(err, "Read")
	}
	if n != 8 {
		return 0, errors.New("prefix has unexpected number of bytes: " + strconv.Itoa(n))
	}

	return int(binary.LittleEndian.Uint64(prefix)), nil
}

func OutputHandler(msg *message.Output, stdout io.Writer) {
	b, err := json.Marshal(msg)
	if err != nil {
		b, err = json.Marshal(message.NewError(errors.Wrap(err, "json.Marshal")))
		if err != nil {
			panic(err)
		}
	}

	if _, err := stdout.Write(PrefixLength(b)); err != nil {
		panic(err)
	}
}

func PrefixLength(data []byte) []byte {
	prefix := make([]byte, 8)
	binary.LittleEndian.PutUint64(prefix, uint64(len(data)))
	return append(prefix, data...)
}
