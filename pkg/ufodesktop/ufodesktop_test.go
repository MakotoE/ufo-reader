package ufodesktop_test

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/MakotoE/ufo-reader/pkg/message"
	"gitlab.com/MakotoE/ufo-reader/pkg/ufodesktop"
	"gitlab.com/MakotoE/ufo-reader/test/testutils"
	"io/ioutil"
	"testing"
)

func TestReadDataLength(t *testing.T) {
	tests := []struct {
		b           []byte
		expected    int
		expectError bool
	}{
		{
			[]byte{0, 0, 0, 0, 0, 0, 0, 0},
			0,
			false,
		},
		{
			[]byte{},
			0,
			true,
		},
		{
			[]byte{0},
			0,
			true,
		},
	}

	for _, test := range tests {
		buffer := &bytes.Buffer{}
		buffer.Write(test.b)
		result, err := ufodesktop.ReadDataLength(buffer)
		assert.Equal(t, test.expected, result)
		testutils.CheckError(t, test.expectError, err)
	}
}

func TestOutputHandler(t *testing.T) {
	buffer := &bytes.Buffer{}

	tests := []struct {
		msg              *message.Output
		expectedData     []byte
		expectedContains []byte
	}{
		{
			nil,
			ufodesktop.PrefixLength([]byte("null")),
			nil,
		},
		{
			message.NewOutput(0, nil),
			ufodesktop.PrefixLength([]byte(`{"ID":"Output","ChainID":0,"Output":null}`)),
			nil,
		},
		{
			message.NewOutput(0, map[interface{}]int{}),
			nil,
			[]byte("unsupported type"),
		},
	}

	for _, test := range tests {
		ufodesktop.OutputHandler(test.msg, buffer)
		result, err := ioutil.ReadAll(buffer)
		buffer.Reset()
		require.Nil(t, err)

		if test.expectedContains == nil {
			assert.Equal(t, test.expectedData, result)
		} else {
			assert.Contains(t, string(result), string(test.expectedContains))
		}
	}
}

func TestPrefixLength(t *testing.T) {
	tests := []struct {
		data     []byte
		expected []byte
	}{
		{[]byte{}, []byte{0, 0, 0, 0, 0, 0, 0, 0}},
		{[]byte{1}, []byte{1, 0, 0, 0, 0, 0, 0, 0, 1}},
	}

	for _, test := range tests {
		assert.Equal(t, test.expected, ufodesktop.PrefixLength(test.data))
	}
}
