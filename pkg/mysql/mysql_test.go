package mysql_test

import (
	"database/sql"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/MakotoE/ufo-reader/pkg/mysql"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"gitlab.com/MakotoE/ufo-reader/test/mocks/mock_mysql"
	"reflect"
	"testing"
	"unsafe"
)

type ColumnTypeFake struct {
	name              string
	hasNullable       bool
	hasLength         bool
	hasPrecisionScale bool
	nullable          bool
	length            int64
	databaseType      string
	precision         int64
	scale             int64
	scanType          reflect.Type
}

func TestMySQL_Read_ZeroRows(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	mockSelectStatement := mock_mysql.NewMockStatement(controller)
	mockRows := mock_mysql.NewMockRows(controller)
	mockSelectStatement.EXPECT().Query(uint(0), uint(0)).Return(mockRows, nil)

	mockRows.EXPECT().ColumnTypes().Return([]*sql.ColumnType{}, nil)
	mockRows.EXPECT().Next().Return(false)

	mysql := mysql.MySQL{nil, mockSelectStatement, nil}
	records, err := mysql.Read(reader.Range1D{0, 0})
	assert.Empty(t, records)
	assert.Nil(t, err)
}

func TestMySQL_Read_OneRow(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	mockStatement := mock_mysql.NewMockStatement(controller)
	mockRows := mock_mysql.NewMockRows(controller)
	mockStatement.EXPECT().Query(uint(1), uint(1)).Return(mockRows, nil)

	columnType := ColumnTypeFake{name: "name", scanType: reflect.TypeOf(sql.NullInt64{})}
	columnTypePtr := (*sql.ColumnType)(unsafe.Pointer(&columnType))
	mockRows.EXPECT().ColumnTypes().Return([]*sql.ColumnType{columnTypePtr}, nil)
	mockRows.EXPECT().Next().Return(true)
	mockRows.EXPECT().Next().Return(false)
	action0 := func(args ...interface{}) {
		args[0].(*sql.NullInt64).Int64 = 1
	}
	mockRows.EXPECT().Scan([]interface{}{&sql.NullInt64{}}).Do(action0).Return(nil)

	mysql := mysql.MySQL{nil, mockStatement, nil}
	records, err := mysql.Read(reader.Range1D{1, 1})
	assert.Equal(t, readresult.Object1D{{"name": &sql.NullInt64{Int64: 1}}}, records)
	assert.Nil(t, err)
}

func TestNames_EmptyTable(t *testing.T) {
	result := mysql.Names(nil)
	assert.Equal(t, []string{}, result)
}

func TestNames_OneColumn(t *testing.T) {
	columnType := ColumnTypeFake{name: "name"}
	pointer := (*sql.ColumnType)(unsafe.Pointer(&columnType))
	result := mysql.Names([]*sql.ColumnType{pointer})
	assert.Equal(t, []string{"name"}, result)
}

func TestScanners_EmptyTable(t *testing.T) {
	result := mysql.Scanners(nil)
	assert.Equal(t, []interface{}{}, result)
}

func TestScanners_OneColumn(t *testing.T) {
	columnType := ColumnTypeFake{scanType: reflect.TypeOf(sql.NullInt64{})}
	pointer := (*sql.ColumnType)(unsafe.Pointer(&columnType))
	types := mysql.Scanners([]*sql.ColumnType{pointer})
	assert.Equal(t, 1, len(types))
	assert.IsType(t, &sql.NullInt64{}, types[0])
}

func TestScanner(t *testing.T) {
	result0 := mysql.Scanner(reflect.TypeOf(sql.NullBool{}))
	assert.IsType(t, &sql.NullBool{}, result0)

	result1 := mysql.Scanner(reflect.TypeOf(sql.NullFloat64{}))
	assert.IsType(t, &sql.NullFloat64{}, result1)

	result2 := mysql.Scanner(reflect.TypeOf(sql.NullInt64{}))
	assert.IsType(t, &sql.NullInt64{}, result2)

	result3 := mysql.Scanner(reflect.TypeOf(sql.NullString{}))
	assert.IsType(t, &sql.NullString{}, result3)

	result4 := mysql.Scanner(reflect.TypeOf(sql.RawBytes{}))
	assert.IsType(t, &sql.RawBytes{}, result4)

	result5 := mysql.Scanner(reflect.TypeOf(nil))
	assert.IsType(t, nil, result5)
}

func TestMySQL_Shape(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	mockCountStatement := mock_mysql.NewMockStatement(controller)
	mockRows := mock_mysql.NewMockRows(controller)
	mockCountStatement.EXPECT().Query().Return(mockRows, nil)

	mockRows.EXPECT().Next().Return(true)
	action := func(args ...interface{}) {
		args[0].(*sql.NullInt64).Int64 = 1
	}
	mockRows.EXPECT().Scan([]interface{}{&sql.NullInt64{}}).Do(action).Return(nil)

	result, err := (&mysql.MySQL{nil, nil, mockCountStatement}).Shape()
	assert.Equal(t, [1]int{1}, result)
	assert.Nil(t, err)
}
