package mysql

//go:generate mockgen -source=mysql.go -destination=../../test/mocks/mock_mysql/mock_mysql.go

import (
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/MakotoE/ufo-reader/pkg/reader"
	"gitlab.com/MakotoE/ufo-reader/pkg/readresult"
	"reflect"
)

var (
	_ reader.ReadFromNone = &MySQL{}
	_ reader.Output1D     = &MySQL{}
)

type MySQL struct {
	Database        *sql.DB
	SelectStatement Statement
	CountStatement  Statement
}

type InitOptions struct {
	mysql.Config `mapstructure:",squash"`
	Table        string
}

func (this *MySQL) Init(options map[string]interface{}) (reader.Reader, error) {
	initOptions := &InitOptions{}
	if err := mapstructure.Decode(options, initOptions); err != nil {
		return nil, errors.Wrap(err, "Decode")
	}

	db, err := sql.Open("mysql", initOptions.FormatDSN())
	if err != nil {
		return nil, errors.Wrap(err, "sql.Open")
	}

	selectStatement, err := db.Prepare("SELECT * FROM " + initOptions.Table + " LIMIT ?,?")
	if err != nil {
		return nil, errors.Wrap(err, "Prepare")
	}

	countStatement, err := db.Prepare("SELECT COUNT(*) FROM " + initOptions.Table)
	if err != nil {
		return nil, errors.Wrap(err, "Prepare")
	}

	return &MySQL{db, &statement{selectStatement}, &statement{countStatement}}, nil
}

func (this *MySQL) Close() {
	_ = this.Database.Close()
}

func (this *MySQL) Read(range1D reader.Range1D) (readresult.Result1D, error) {
	rows, err := this.SelectStatement.Query(range1D.From, range1D.Size)
	if err != nil {
		return nil, errors.Wrap(err, "Query")
	}

	columnTypes, err := rows.ColumnTypes()
	if err != nil {
		return nil, errors.Wrap(err, "ColumnTypes")
	}

	names := Names(columnTypes)

	var records readresult.Object1D

	for rows.Next() {
		fields := Scanners(columnTypes)

		if err = rows.Scan(fields...); err != nil {
			return nil, errors.Wrap(err, "Scan")
		}

		record := make(map[string]interface{})
		for i, name := range names {
			record[name] = fields[i]
		}

		records = append(records, record)
	}

	return records, nil
}

func Names(columns []*sql.ColumnType) []string {
	names := make([]string, len(columns))
	for i, column := range columns {
		names[i] = column.Name()
	}
	return names
}

func Scanners(columns []*sql.ColumnType) []interface{} {
	scanners := make([]interface{}, len(columns))
	for i, column := range columns {
		scanner := Scanner(column.ScanType())
		scanners[i] = scanner
	}
	return scanners
}

func Scanner(t reflect.Type) interface{} {
	switch t {
	case reflect.TypeOf(sql.NullBool{}):
		return &sql.NullBool{}
	case reflect.TypeOf(sql.NullFloat64{}):
		return &sql.NullFloat64{}
	case reflect.TypeOf(sql.NullInt64{}):
		return &sql.NullInt64{}
	case reflect.TypeOf(sql.NullString{}):
		return &sql.NullString{}
	case reflect.TypeOf(sql.RawBytes{}):
		return &sql.RawBytes{}
	default:
		return nil
	}
}

func (this *MySQL) Shape() ([1]int, error) {
	result, err := this.CountStatement.Query()
	if err != nil {
		return [1]int{}, errors.Wrap(err, "Query")
	}

	result.Next()

	count := &sql.NullInt64{}
	if err := result.Scan(count); err != nil {
		return [1]int{}, errors.Wrap(err, "Scan")
	}

	return [1]int{int(count.Int64)}, nil
}

type Rows interface {
	ColumnTypes() ([]*sql.ColumnType, error)
	Next() bool
	Scan(...interface{}) error
}

type Statement interface {
	Query(args ...interface{}) (Rows, error)
}

type statement struct {
	statement *sql.Stmt
}

func (this *statement) Query(args ...interface{}) (Rows, error) {
	return this.statement.Query(args...)
}
